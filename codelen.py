import os

def count(cur):
  print(cur)
  if cur.endswith('.java'):
    print('counted ' + cur)
    f = open(cur, 'r')
    lines = [line.strip() for line in f.readlines()]
    f.close()
    return len(lines)
  elif os.path.isdir(cur):
    total = 0
    for fn in os.listdir(cur):
      total += count(cur + '/' + fn)
    return total
  return 0
 
print(count('src'))
