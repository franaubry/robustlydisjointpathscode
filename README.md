# RobustlyDisjointPathsCode

## Compiling the code

### Using with eclipse:

If you have eclipse installed, you can import the root directory of the project as an eclipse project and the run the code from there.

### Without eclipse:

#### To compile execute:

Just run (you will need to have Java installed):

```
./comiple
```

#### To run the code execute:

```
./run.sh path_to_topology path_to_demands max_seg failure_type max_cores
```

- path_to_topology: the path to the network topology
- path_to_demands:  the path to the demand file (source-destinations tuples)
- max_seg:          the maximum number of segments allowed on the paths
- failure_type:     either single_link or srlg (random 3-edge sets)
- max_cores:        the maximum number of demands processed at the same time (one thread per demand)

##### Example:

./run.sh ../topologies/rf/1755.ntfl ../demands/rf/1755.dem 4 single_link 1

Will run on the **Rocketfuel topology 1755** with the **demands file1755.dem** finding paths up to **4** segments that
are robustly disjoint to **single link failures** and **running only 1 demand at a time**.

The results will be written in ./results/runDemands/[topology_name]_[max_seg]_[failure_type].json

So the above command will write the results in the file *./results/runDemands/1755_4_single_link.json*
