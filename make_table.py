import json
import os
from openpyxl import *
from openpyxl import Workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors
from openpyxl.cell import Cell

#############
# UTILS
#############

ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def get_cell_lbl(ridx, cidx):
  return get_col_lbl(cidx) + str(ridx + 1)

def get_col_lbl(cidx):
  ans = [ ]
  while cidx >= len(ALPHABET):
    r = int(cidx % len(ALPHABET))
    ans.append(ALPHABET[r])
    cidx //= len(ALPHABET)
  if len(ans) > 0:
    ans.append(ALPHABET[cidx - 1])
  else:
    ans.append(ALPHABET[cidx])
  ans.reverse()
  return ''.join(ans)

def get_value(sheet, row, col):
  return sheet[get_cell_lbl(row, col)].value

def set_value(sheet, row, col, value):
  sheet[get_cell_lbl(row, col)] = value

##############

def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

def make_nb_seg_table(topology, maxseg):
  data = read_json('./results/runDemands/{0}_{1}.json'.format(topology, maxseg))
  rdp_seg_cnt = { }
  suur_seg_cnt = { }
  for res in data['demands']:
    if res['pathsExist']:
      rdp_seg1 = len(res['path1'])
      rdp_seg2 = len(res['path2'])
      rdp_seg = [ rdp_seg1, rdp_seg2 ]
      for seg in rdp_seg:
        if seg not in rdp_seg_cnt:
          rdp_seg_cnt[seg] = 1
        else:
          rdp_seg_cnt[seg] += 1
    suurballe_nb_paths = res['suurballeNbPaths']
    if suurballe_nb_paths == 2:
      suur_seg = [ res['suurballePaths']['nbSeg0'], res['suurballePaths']['nbSeg1'] ]
      for seg in suur_seg:
        if seg not in suur_seg_cnt:
          suur_seg_cnt[seg] = 1
        else:
          suur_seg_cnt[seg] += 1
  print(rdp_seg_cnt)
  print(suur_seg_cnt)

def check_suurballe(topology, maxseg):
  data = read_json('./results/runDemands/{0}_{1}.json'.format(topology, maxseg))
  x = [ 0 for i in range(4) ]
  for res in data['demands']:
    suurballe_nbp = res['suurballeNbPaths']
    x[suurballe_nbp] += 1
  print(x)

def autofit(ws):
  for col in ws.columns:
     max_length = 0
     column = col[0].column # Get the column name
     for cell in col:
         try: # Necessary to avoid error on empty cells
             if len(str(cell.value)) > max_length:
                 max_length = len(cell.value)
         except:
             pass
     adjusted_width = (max_length + 2) * 1.2
     ws.column_dimensions[column].width = adjusted_width

def make_rdp_table(wb, max_seg, failure):
  ws = wb.create_sheet(title=failure)
  row = 0
  L = os.listdir('topologies/zoo_large/')
  L += os.listdir('topologies/rf/')
  L += os.listdir('topologies/real/')
  for fn in L:
    set_value(ws, row, 1, 'number of segments')
    set_value(ws, row, 2, 'failure scenario')
    set_value(ws, row, 3, '% of demands with RDP')
    set_value(ws, row, 4, '% of demands with Suurballe')
    #set_value(ws, 0, 4, 'average minimum cut')
    set_value(ws, row, 5, 'average nominal latency')
    set_value(ws, row, 6, 'average RDP latency')
    set_value(ws, row, 7, 'average latency ratio')
    set_value(ws, row, 8, 'average suurballe latency')
    set_value(ws, row, 9, 'average suurballe segments')
    set_value(ws, row, 10, 'average runtime (s)')
    row += 1
    for max_seg in range(2, 6):
      name = fn.split('.')[0]
      res_fn = 'results/runDemands/{0}_{1}_{2}.json'.format(name, max_seg, failure)
      if not os.path.exists(res_fn):
        print('could not find results for ' + name)
        continue
      data = read_json('results/runDemands/{0}_{1}_{2}.json'.format(name, max_seg, failure))
      # init data
      rdp_cnt = 0
      suurballe_cnt = 0
      #min_cut_sum = 0
      nom_lat_sum = 0
      rdp_lat_sum = 0
      lat_ratio = 0
      suurballe_lat_sum = 0
      runtime_sum = 0
      suurballe_seg_sum = 0
      # compute data
      for res in data['demands']:
        if res['pathsExist']: 
          rdp_cnt += 1
          rdp_lat_sum += res['maxlat']
          lat_ratio += res['maxlat'] / res['maxNomLat']
        if res['suurballeNbPaths'] == 2:
          suurballe_cnt += 1
          suurballe_lat1 = res['suurballePaths']['lat0']
          suurballe_lat2 = res['suurballePaths']['lat1']
          seg1 = res['suurballePaths']['nbSeg0']
          seg2 = res['suurballePaths']['nbSeg1']
          suurballe_seg_sum += max(seg1, seg2)
        nom_lat_sum += res['maxNomLat']
        suurballe_lat_sum += max(suurballe_lat1, suurballe_lat2)
        runtime_sum += res['runtime'] / 1e9
      # add data to sheet
      n = len(data['demands'])
      set_value(ws, row, 0, name)
      set_value(ws, row, 1, max_seg)
      set_value(ws, row, 2, failure)
      set_value(ws, row, 3, int(100.0 * rdp_cnt / n))
      set_value(ws, row, 4, int(100.0 * suurballe_cnt / n))
      #set_value(ws, 0, 4, 'average minimum cut')
      set_value(ws, row, 5, nom_lat_sum / n)
      set_value(ws, row, 6, int(rdp_lat_sum / n))
      set_value(ws, row, 7, lat_ratio / n)
      set_value(ws, row, 8, int(suurballe_lat_sum / n))
      set_value(ws, row, 9, 2 + suurballe_seg_sum / n)
      set_value(ws, row, 10, runtime_sum / n)
      row += 1
    autofit(ws)

def make_suurballe_table(wb):
  ws = wb.create_sheet(title="suurballe")
  set_value(ws, 0, 1, 'average number of segments')
  set_value(ws, 0, 2, 'max seg')
  set_value(ws, 0, 3, '% of demands with solution')
  set_value(ws, 0, 4, 'average nominal latency')
  set_value(ws, 0, 5, 'average latency')
  set_value(ws, 0, 6, 'average latency difference')
  set_value(ws, 0, 7, 'average latency ratio')
  set_value(ws, 0, 8, 'average min-cut')
  set_value(ws, 0, 9, 'average runtime (s)')
  row = 1
  L = os.listdir('topologies/zoo_large/')
  L += os.listdir('topologies/rf/')
  L += os.listdir('topologies/real/')
  for fn in L:
    name = fn.split('.')[0]
    res_fn = 'results/suurballe/{0}.json'.format(name)
    if not os.path.exists(res_fn):
      print('could not find results for ' + name)
      continue
    data = read_json('results/suurballe/{0}.json'.format(name))
    # init data
    nb_seg_cnt = 0
    suurballe_cnt = 0
    nom_lat_sum = 0
    lat_sum = 0
    lat_dif = 0
    cut_sum = 0
    runtime_sum = 0
    max_seg = 0
    lat_ratio = 0
    # compute data
    for res in data['demands']:
      if res['suurballeNbPaths'] == 2:
        suurballe_cnt += 1
        suurballe_lat1 = res['suurballePaths']['lat0']
        suurballe_lat2 = res['suurballePaths']['lat1']
        seg1 = res['suurballePaths']['nbSeg0']
        seg2 = res['suurballePaths']['nbSeg1']
        max_seg = max(max_seg, max(seg1, seg2))
        nb_seg_cnt += max(seg1, seg2)
        lat_sum += max(suurballe_lat1, suurballe_lat2)
        lat_dif += max(suurballe_lat1, suurballe_lat2) - res['maxNomLat']
        lat_ratio += max(suurballe_lat1, suurballe_lat2) / res['maxNomLat']
      cut_sum += res['mincut']
      nom_lat_sum += res['maxNomLat']
      runtime_sum += res['runtime'] / 1e9
    # add data to sheet
    n = data['nbDemands']
    set_value(ws, row, 0, name)
    set_value(ws, row, 1, nb_seg_cnt / suurballe_cnt)
    set_value(ws, row, 2, max_seg)
    set_value(ws, row, 3, int(100 * suurballe_cnt / n))
    set_value(ws, row, 4, int(nom_lat_sum / n))
    set_value(ws, row, 5, int(lat_sum / n))
    set_value(ws, row, 6, int(lat_dif / n))
    set_value(ws, row, 7, lat_ratio / n)
    set_value(ws, row, 8, cut_sum / n)
    set_value(ws, row, 9, runtime_sum / n)
    row += 1
  autofit(ws)

def make_paper_table(wb, fail):
  ws = wb.create_sheet(title="paper-" + fail)
  set_value(ws, 1, 0, 'topology')
  set_value(ws, 1, 1, '0 det')
  set_value(ws, 1, 2, '1 det')
  set_value(ws, 1, 3, '2 det')
  set_value(ws, 1, 4, '3 det')
  set_value(ws, 1, 5, '0 det')
  set_value(ws, 1, 6, '1 det')
  set_value(ws, 1, 7, '2 det')
  set_value(ws, 1, 8, '3 det')
  set_value(ws, 1, 9, '0 det')
  set_value(ws, 1, 10, '1 det')
  set_value(ws, 1, 11, '2 det')
  set_value(ws, 1, 12, '3 det')

  ws.merge_cells(start_row=1, start_column=2, end_row=1, end_column=5)
  set_value(ws, 0, 1, 'src-dst tuples with RDPs')
  ws.merge_cells(start_row=1, start_column=6, end_row=1, end_column=9)
  set_value(ws, 0, 5, 'latency ratio wrt IGP')
  ws.merge_cells(start_row=1, start_column=10, end_row=1, end_column=13)
  set_value(ws, 0, 9, 'RDPs computation time')

  L = [ ]
  L += os.listdir('topologies/zoo_large/')
  L += os.listdir('topologies/rf/')
  L += os.listdir('topologies/real/')
  row = 2
  for fn in L:
    name = fn.split('.')[0]
    set_value(ws, row, 0, name)
    # do percentage table
    col = 1
    prev_sol = 0
    for max_seg in range(2, 6):
      res_fn = 'results/runDemands/{0}_{1}_{2}.json'.format(name, max_seg, fail)
      if not os.path.exists(res_fn):
        print('could not find results for ' + name)
        continue
      data = read_json('results/runDemands/{0}_{1}_{2}.json'.format(name, max_seg, fail))
      rdp_cnt = 0
      lat_ratio = 0
      time = 0
      cnt = 0
      for res in data['demands']:
        if cnt == 100: break;
        if res['pathsExist']: 
          rdp_cnt += 1
          lat_ratio += res['maxlat'] / res['maxNomLat']
        time += res['runtime'] / 1e9
        cnt += 1;
      n = 100
      sol = int(100 * rdp_cnt / n)
      set_value(ws, row, col, sol)
      if rdp_cnt > 0:
        set_value(ws, row, col + 4, "%.2f" % (lat_ratio / rdp_cnt))
      else:
        set_value(ws, row, col + 4, "n.a.")
      set_value(ws, row, col + 8, time / n)
      prev_sol = sol
      col += 1
    row += 1
  autofit(ws)

if __name__ == '__main__':
  wb = Workbook()
  make_paper_table(wb, 'single_link')
  make_paper_table(wb, 'srlg')
  #make_rdp_table(wb, 5, 'single_link')
  #make_rdp_table(wb, 5, 'srlg')
  #make_suurballe_table(wb)
  del wb['Sheet']
  wb.save('results.xlsx')

