import os
import json
import subprocess
import time

def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

for dir in ['rf', 'real', 'zoo_large']:
  for fn in os.listdir('topologies/' + dir):
    name = fn.split('.')[0]
    print(name)
    data = read_json('./results/runDemands/{0}_5_single_link.json'.format(name))
    f = open('tmp/{0}.tmp'.format(name), 'w')
    for res in data['demands']:
      if res['pathsExist']:
        p1 = ' '.join(res['path1'])
        p2 = ' '.join(res['path2'])
        f.write(p1 + '\n')
        f.write(p2 + '\n')
    f.close()
    """
    cmd = 'java -jar simulate.jar topologies/{0}/{1} {2}.tmp 1 6 random 100 > out.txt 2> err.txt'.format(dir, fn, name)
    os.system(cmd)
    print(cmd)
    while True:
      if os.path.exists('out.txt'):
        f = open('out.txt', 'r')
        lines = [line.strip() for line in f.readlines()]
        if len(lines) > 0:
          if lines[-1] == 'done':
            break
          else:
            print(lines[-1])
      time.sleep(1)
     """
