import os
import sys
import time

dir = sys.argv[1]
maxseg = int(sys.argv[2])
cores = int(sys.argv[3])

"""
if os.path.exists('./logs'):
  os.system('rm -r logs')
os.system('mkdir logs')
"""

L = os.listdir('topologies/' + dir)

for fn in L:
  name = fn.split('.')[0]
  if os.path.exists('./logs/log_{0}.txt'.format(name)):
    f = open('logs/log_{0}.txt'.format(name), 'r')  
    lines = [line.strip() for line in f.readlines()]
    if len(lines) > 0 and lines[-1].startswith('done'):
      print('skip ' + name)
      continue
    else:
      os.system('rm ./logs/log_{0}.txt'.format(name))
  if not os.path.exists('./logs/log_{0}.txt'.format(name)):
    cmd = 'java -jar run_parallel.jar topologies/{3}/{4} demands/{3}/{0}.dem {1} single_link {2} > logs/log_{0}.txt 2> logs/err_{0}.txt &'.format(name, maxseg, cores, dir, fn)
    print('running: ' + name)
    os.system(cmd)
    while True:
      while not os.path.exists('logs/log_{0}.txt'.format(name)):
        time.sleep(1)
      f = open('logs/log_{0}.txt'.format(name), 'r')
      lines = [line.strip() for line in f.readlines()]
      if len(lines) > 0 and lines[-1].startswith('done'):
        break
      elif len(lines) > 0:
        print(lines[-1])
      time.sleep(1)
