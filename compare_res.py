import json
import sys

def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

data1 = read_json(sys.argv[1])
data2 = read_json(sys.argv[2])

st1 = set()
for r in data1['demands']:
  st1.add( (r['s1'], r['s2'], r['t1'], r['t2']) )

st2 = set()
for r in data2['demands']:
  st2.add( (r['s1'], r['s2'], r['t1'], r['t2']) )


for x in st1:
  if not x in st2:
    print('st1 not containted in st2')
    break

for x in st2:
  if not x in st1:
    print('st2 not contained in st1')
    break
