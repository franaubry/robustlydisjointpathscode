import os
import sys
import time

cores = int(sys.argv[1])
fail = sys.argv[2]

for maxseg in [5, 4, 3, 2]:
  for dir in ['rf', 'real', 'zoo_large']:
    L = os.listdir('topologies/' + dir)
    for fn in L:
      name = fn.split('.')[0]
      if os.path.exists('./logs/log_{0}_{1}.txt'.format(name, maxseg)):
        f = open('logs/log_{0}_{1}.txt'.format(name, maxseg), 'r')  
        lines = [line.strip() for line in f.readlines()]
        if len(lines) > 0 and lines[-1].startswith('done'):
          print('skip ' + name)
          continue
        else:
          os.system('rm ./logs/log_{0}_{1}.txt'.format(name, maxseg))
      if not os.path.exists('./logs/log_{0}_{1}.txt'.format(name, maxseg)):
        cmd = './jre1.8.0_121/bin/java -jar run_parallel_wrdp.jar topologies/{3}/{4} demands/{3}/{0}.dem {1} {5} {2} > logs/log_{0}_{1}.txt 2> logs/err_{0}_{1}.txt &'.format(name, maxseg, cores, dir, fn, fail)
        print(cmd)
        print('running: ' + name)
        os.system(cmd)
        while True:
          while not os.path.exists('logs/log_{0}_{1}.txt'.format(name, maxseg)):
            time.sleep(1)
          f = open('logs/log_{0}_{1}.txt'.format(name, maxseg), 'r')
          lines = [line.strip() for line in f.readlines()]
          if len(lines) > 0 and lines[-1].startswith('done'):
            break
          elif len(lines) > 0:
            print(lines[-1])
          time.sleep(1)
