import os
import json

def read_json(dir, fn, max_seg, fail):
  f = open('topologies/' + dir + '/' + fn.split('.')[0] + '.json', 'r')
  json_data = f.read()
  return json.loads(json_data)

def read_demands(dir, fn):
  f = open('demands/' + dir + '/' + fn.split('.')[0] + '.dem', 'r')
  demands = [ line.strip().split(' ') for line in f.readlines() ]
  s = set()
  for d in demands:
    s.add( tuple(d) )
  return s

def clean(dir, fn, max_seg, fail):
  data = read_json(dir, fn, max_seg, fail)
  demands = read_demands(dir, fn)
  print(len(demands))
  cnt = 0
  for res in data['demands']:
    d = (res['s1'], res['s2'], res['t1'], res['t2'])
    if d in demands:
      cnt += 1
  print(cnt)

L = [ ]
L += os.listdir('topologies/zoo_large/')
L += os.listdir('topologies/rf/')
L += os.listdir('topologies/real/')

clean('zoo_large', 'GtsCe.graph', 5, 'single_link')
