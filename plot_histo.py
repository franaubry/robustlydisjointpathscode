import matplotlib
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(19680801)

f = open('results/minseg/all.txt', 'r')
lines = [line.strip() for line in f.readlines()]
x = [ int(line.split(' ')[0]) for line in lines ]
y = [ float(line.split(' ')[1]) for line in lines ]

print(x)
print(y)

split = 5

fail = 0
for i in range(split, len(y)):
  fail += y[i]

print(fail)

plt.text(6.6, 15, '26.6% for the pairs', fontsize=16)

plt.bar( [ x[i] for i in range(0, split) ] , [ y[i] for i in range(0, split) ], color='#339966')

plt.bar( [ x[i] for i in range(split, len(x)) ] , [ y[i] for i in range(split, len(x)) ], color='#ff5050')

plt.xticks([ a + 0.4 for a in x ], x, fontsize=12)

plt.xlabel('Number of segments', fontsize=16)
plt.ylabel('Percentage of pairs', fontsize=16)

# Tweak spacing to prevent clipping of ylabel
#fig.tight_layout()
plt.title('Number of segments in min latency paths - OVH Europe')

plt.savefig('./plots/minseg.eps', format='eps', dpi=300, bbox_inches='tight')
