#!/usr/local/bin/python
from graphviz import Digraph
from graphviz import Source
import os
import argparse
import pydot

def parse_zoo(fn):
  f = open(fn, 'r')
  lines = [line.strip() for line in f.readlines()]
  data = lines[0].split(' ')
  V = int(data[1])
  data = lines[V + 3].split(' ')
  E = int(data[1])
  edges = [ ]
  for i in range(V + 5, V + 5 + E):
    data = lines[i].split(' ')
    orig = int(data[1])
    dest = int(data[2])
    edges.append( (orig, dest) )
  return V, edges

DIRS = ['zoo', 'zoo_large']

for d in DIRS:
  path = '../topologies/{0}/'.format(d)
  for fn in os.listdir(path):
    print('plotting {0}'.format(fn))
    V, edges = parse_zoo(path + fn)
    graph = pydot.Dot()
    graph.set_type('digraph')
    for v in range(V):
      graph.add_node(pydot.Node(v))
    for u, v in edges:
      graph.add_edge(pydot.Edge(u, v))
    graph.write_pdf('./{0}/{1}.pdf'.format(d, fn.split('.')[0]))

