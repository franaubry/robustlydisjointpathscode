import json
import os

def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

def readlines(fn):
  if not os.path.exists(fn): return [ ]
  f = open(fn, 'r')
  lines = [ line.strip() for line in f.readlines() ]
  f.close()
  return lines

if os.path.exists('./tmp'): os.system('rm -r ./tmp')
os.system('mkdir tmp')

for dir in ['rf', 'real', 'zoo_large']:
  for fn in os.listdir('topologies/' + dir):
    print('checking: ' + fn)
    name = fn.split('.')[0]
    data = read_json('results/runDemands/{0}_5_srlg.json'.format(name))
    for res in data['demands']:
      if not res['pathsExist']: continue
      p1 = ' '.join(res['path1'])
      p2 = ' '.join(res['path2'])
      if os.path.exists('./tmp/check.txt'): os.system('rm ./tmp/check.txt')
      if os.path.exists('./tmp/check_err.txt'): os.system('rm ./tmp/check_err.txt')
      cmd = 'java -jar verify_srlg.jar {0} "{1}" "{2}" > tmp/check.txt 2> tmp/check_err.txt'.format('topologies/{1}/{0}'.format(fn, dir), p1, p2)
      os.system(cmd)
      lines = readlines('./tmp/check_err.txt')
      if len(lines) > 0:
        print('error in verifier')
        os.system('cat ./tmp/check_err.txt')
        exit(0)
      lines = readlines('./tmp/check.txt')
      if lines[0] != 'ok':
        print('rpd is not correct')
        os.system('cat ./tmp/check.txt')
        exit(0)

print('all ok')
