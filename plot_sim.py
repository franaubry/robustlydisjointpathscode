import numpy as np
import matplotlib.pyplot as plt
import json
import os

AXIS_FONTSIZE = 26
TICKS_FONTSIZE = 26

plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42


def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

for fn in os.listdir('results/simulation/'):
  name = fn.split('.')[0]
  data = read_json('results/simulation/' + fn)
  types = ['both_disconnected', 'one_disconnected', 'not_disjoint', 'disjoint' ]

  for fail in ['random', 'worst_init', 'worst']:
    percents = [ [0 for _ in range(6) ] for _ in range(4) ]
    for i in range(4):
      for j in range(6):
        percents[i][j] = float(data[fail][str(j + 1)][types[i]])
      print(percents)
    print()
    # data to plot
    n_groups = 6
    both_disconnected = percents[0]
    one_disconnected = percents[1]
    not_disjoint = percents[2]
    disjoint = percents[3]

    # create plot
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.2
    opacity = 0.8
 
    rects1 = plt.bar(index, both_disconnected, bar_width,
                 alpha=opacity,
                 color='midnightblue',
                 label='both disconnected')
 
    rects2 = plt.bar(index + bar_width, one_disconnected, bar_width,
                 alpha=opacity,
                 color='steelblue',
                 label='one disconnected')

    rects3 = plt.bar(index + 2 * bar_width, not_disjoint, bar_width,
                 alpha=opacity,
                 color='dodgerblue',
                 label='not disjoint')

    rects4 = plt.bar(index + 3 * bar_width, disjoint, bar_width,
                 alpha=opacity,
                 color='skyblue',
                 label='disjoint')


    
    plt.yticks(fontsize=TICKS_FONTSIZE)

   


    plt.xlabel('Number of simultaneous failures', fontsize=AXIS_FONTSIZE)
    plt.ylabel('Fraction of experiments', fontsize=AXIS_FONTSIZE)
    plt.xticks(index + bar_width, ('1', '2', '3', '4', '5', '6'), fontsize=TICKS_FONTSIZE)
    plt.legend(fontsize=24)
 
    plt.tight_layout()
    plt.savefig('./plots/{1}_{0}.eps'.format(fail, name), format='eps', dpi=300, bbox_inches='tight')
    plt.savefig('./plots/{1}_{0}.png'.format(fail, name), format='png', dpi=300, bbox_inches='tight')
 
