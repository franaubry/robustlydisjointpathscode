import os
import sys

"""
for fn in os.listdir('topologies/rf/'):
  name = fn.split('.')[0]
  cmd = 'java -jar run.jar topologies/rf/{0}.ntfl demands/rf/{0}.dem 3 > logs/log_{0}.txt &'.format(name)
  os.system(cmd)
"""

maxseg = int(sys.argv[1])

if os.path.exists('./logs'):
  os.system('rm -r logs')
os.system('mkdir logs')

for fn in os.listdir('topologies/zoo_large/'):
  name = fn.split('.')[0]
  print(name)
  cmd = './jre1.8.0_121/bin/java -jar run.jar topologies/zoo/{0}.graph demands/zoo/{0}.dem {1} > logs/log_{0}.txt &'.format(name, maxseg)
  os.system(cmd)
