package test;

import org.junit.Test;

import dataStructures.RDPDemand;
import graphs.Graph;
import io.DemandIO;
import io.GraphIO;
import rdp.FailureGen;
import rdp.RDPGeneral;
import rdp.RDPOneSidedFailures;
import rdp.RDPSolution;
import util.Timer;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;


public class RDPvsRDPOneSided {

	@Test
	public void compareOnRFSingleEdgeFailures() {
		System.out.println("----------");
		System.out.println("test: compare single edge failure on RF topologies");
		File dir = new File("./topologies/rf");
		for(String f : dir.list()) {
			System.out.println("testing on: " + f);
			Graph g = GraphIO.read("./topologies/rf/" + f);
			ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, "./demands/rf/" + g.getName() + ".dem");
			BitSet[] singleEdge = FailureGen.genAllEdges(g);
			RDPGeneral rdpGen = new RDPGeneral(g, singleEdge);
			RDPOneSidedFailures rdpOne = new RDPOneSidedFailures(g, singleEdge);
			Timer timer = new Timer();
			for(int i = 0; i < demands.size(); i++) {
				int percent = (int)(100.0 * i / demands.size());
				System.out.println(i + ", " + percent + "%");
				timer.createAndStart("solgen");
				RDPSolution solGen = rdpGen.findPaths(demands.get(i), 3);
				timer.stop("solgen");
				timer.createAndStart("solone");
				RDPSolution solOne = rdpOne.findPaths(demands.get(i), 3);
				timer.stop("solone");
				assertTrue(solGen.rdpExist() == solOne.rdpExist());
				double lat1 = solGen.maxLat();
				double lat2 = solGen.maxLat();
				assertTrue(Math.abs(lat1 - lat2) <= 1e6);
				System.out.println(timer.getSeconds("solgen") + " " + timer.getSeconds("solone"));
			}
		}
		System.out.println("---------");
	}
	
}
