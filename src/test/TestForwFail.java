package test;

import java.io.File;
import java.util.Arrays;
import java.util.BitSet;

import org.junit.Test;

import graphs.Graph;
import graphs.Path;
import io.GraphIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;
import verification.Tools;

import static org.junit.Assert.*;


public class TestForwFail {
	
	@Test
	public void test1() {
		Graph g = GraphIO.read("topologies/zoo/GtsCe.graph");
		BitSet[] F = FailureGen.genAllEdges(g);
		ForwFailOneSidedFailures ffs = new ForwFailOneSidedFailures(g, F);
		
		Path p1 = new Path();
		p1.add(92); p1.add(35); p1.add(15); p1.add(81);
		BitSet forwp1 = ffs.forw(p1);
		forwp1.xor(Tools.computeForw(g, p1));
		assertTrue(forwp1.cardinality() == 0);
		
		Path p2 = new Path();
		p2.add(96); p2.add(64); p2.add(118); p2.add(119);
		BitSet forwp2 = ffs.forw(p2);
		forwp2.xor(Tools.computeForw(g, p2));
		assertTrue(forwp2.cardinality() == 0);	
	}

	/*
	
	@Test
	public void testZooSingleEdge() {
		File dir = new File("topologies/zoo/");
		String[] list = dir.list();
		Arrays.sort(list);
		for(String fn : list) {
			System.out.println("testing: " + fn);
			Graph g = GraphIO.read("topologies/zoo/" + fn);
			BitSet[] F = FailureGen.genAllEdges(g);
			ForwFailOneSidedFailures ffs = new ForwFailOneSidedFailures(g, F);
			for(int x = 0; x < g.V(); x++) {
				for(int y = 0; y < g.V(); y++) {
					BitSet forw = Tools.computeForw(g, x, y);
					forw.xor(ffs.forw(x, y));
					assertTrue(forw.cardinality() == 0);
				}
			}
		}
	}
	
	*/

	
}
