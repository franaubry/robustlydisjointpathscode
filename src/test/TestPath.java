package test;

import graphs.Path;

public class TestPath {
	
	public static void main(String[] args) {
		Path path = new Path();
		path.add(1);
		path.add(2);
		path.add(3);
		path.add(4);
		System.out.println(path);
		path.removeLast();
		System.out.println(path);
		path.add(5);
		System.out.println(path);
		path.reverse();
		System.out.println(path);
		path.removeLast();
		System.out.println(path);
		path.reverse();
		System.out.println(path);
	}

}
