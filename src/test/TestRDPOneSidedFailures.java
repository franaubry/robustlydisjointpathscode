package test;

import java.util.BitSet;

import dataStructures.RDPDemand;
import graphs.Graph;
import graphs.Path;
import io.FailureIO;
import io.GraphIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;
import rdp.RDPOneSidedFailures;
import rdp.RDPSolution;
import verification.Tools;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestRDPOneSidedFailures {
	
	@Test
	public void test4() {
		Graph g = GraphIO.read("topologies/zoo/Deltacom.graph");
		BitSet[] F = FailureGen.genAllEdges(g);
		RDPDemand demand = new RDPDemand(7, 73, 3, 43);
		RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, F);
		RDPSolution sol = rdp.findPaths(demand, 4);
		Path p1 = sol.getP1();
		Path p2 = sol.getP2();
		ForwFailOneSidedFailures ffs = rdp.getFFS();
		BitSet forw1 = ffs.forw(p1);
		BitSet forw2 = ffs.forw(p2);
		assertTrue(!forw1.intersects(forw2));
		BitSet fail1 = ffs.fail(p1);
		BitSet fail2 = ffs.fail(p2);
		assertTrue(!forw1.intersects(fail2));
		assertTrue(!fail1.intersects(forw2));		
		assertTrue(Tools.areRobustlyDisjointFailUnion(g, p1, p2, F));
		assertTrue(Tools.areRobustlyDisjoint(g, p1, p2, F));
	}
	
	/*
	
	@Test
	public void test1() {
		Graph g = GraphIO.read("topologies/tests/2failures_1.g");
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_1.fail");
		RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(!sol.rdpExist());
	}
	
	@Test
	public void test2() {
		Graph g = GraphIO.read("topologies/tests/2failures_2.g");
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_1.fail");
		RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(sol.rdpExist());
	}
	
	@Test
	public void test3() {
		Graph g = GraphIO.read("topologies/tests/2failures_2.g");
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_2.fail");
		RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(!sol.rdpExist());
	}
	
	*/




}
