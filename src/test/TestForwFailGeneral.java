package test;

import org.junit.Test;

import graphs.Graph;
import io.FailureIO;
import io.GraphIO;
import rdp.ForwFailGeneral;

import static org.junit.Assert.*;

import java.util.BitSet;

public class TestForwFailGeneral {

	@Test
	public void test1() {
		Graph g = GraphIO.read("topologies/tests/2failures_2.g", true, true);
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_1.fail");
		ForwFailGeneral ffs = new ForwFailGeneral(g, F);
		assertTrue(ffs.forw(g.getIndex("A"), g.getIndex("B")).toString().equals("{0, 15}"));
		assertTrue(ffs.forw(g.getIndex("A"), g.getIndex("H")).toString().equals("{0, 1, 2, 3, 4, 6, 8, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 23, 25, 26, 27, 29}"));
	}
	
}
