package test;

import java.util.BitSet;

import dataStructures.RDPDemand;
import graphs.Graph;
import io.FailureIO;
import io.GraphIO;
import rdp.RDPGeneral;
import rdp.RDPSolution;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestRDP {

	@Test
	public void test1() {
		Graph g = GraphIO.read("topologies/tests/2failures_1.g", true, true);
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_1.fail");
		RDPGeneral rdp = new RDPGeneral(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(!sol.rdpExist());
	}
	
	@Test
	public void test2() {
		Graph g = GraphIO.read("topologies/tests/2failures_2.g", true, true);
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_1.fail");
		RDPGeneral rdp = new RDPGeneral(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(sol.rdpExist());
	}
	
	@Test
	public void test3() {
		Graph g = GraphIO.read("topologies/tests/2failures_2.g", true, true);
		RDPDemand demand = new RDPDemand(g, "A", "A", "H", "H");
		BitSet[] F = FailureIO.readFailures(g, "failures/tests/2failures_2.fail");
		RDPGeneral rdp = new RDPGeneral(g, F);
		RDPSolution sol = rdp.findPaths(demand, 3);
		assertTrue(sol.rdpExist());
	}


}
