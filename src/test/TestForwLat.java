package test;

import java.util.BitSet;

import graphs.EdgeIndexMapper;
import graphs.Graph;
import io.GraphIO;
import rdp.FailureSet;
import rdp.ForwFailGeneral;

public class TestForwLat {
	
	public static void main(String[] args) {
		test1();
	}
	
	static void test1() {
		Graph g = GraphIO.read("topologies/tests/2failures2.g", true, true);
		EdgeIndexMapper em = new EdgeIndexMapper(g);
		FailureSet f = new FailureSet();
		f.add(em.getEdgeBetween("B", "E"));
		f.add(em.getEdgeBetween("D", "G"));
		BitSet[] F = new BitSet[1];
		F[0] = f.bitset();
		ForwFailGeneral ffs = new ForwFailGeneral(g, F);
		System.out.print("  ");
		for(int i = 0; i < g.V(); i++) {
			System.out.print(g.getLabel(i));
			System.out.print(" ");
		}
		System.out.println();
		for(int i = 0; i < g.V(); i++) {
			System.out.print(g.getLabel(i) + " ");
			for(int j = 0; j < g.V(); j++) {
				System.out.print((int)ffs.forwLat(i, j) + " ");
			}
			System.out.println();
		}
	}

}
