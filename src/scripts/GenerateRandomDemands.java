package scripts;

import java.io.File;
import java.util.ArrayList;

import dataStructures.RDPDemand;
import generation.DemandGenerator;
import graphs.Graph;
import io.DemandIO;
import io.GraphIO;

public class GenerateRandomDemands {
	
	public static void main(String[] args) {
		File dir = new File("topologies/real/");
		for(String fn : dir.list()) {
			System.out.println("generating demands for " + fn);
			Graph g = GraphIO.read("topologies/real/" + fn);
			ArrayList<RDPDemand> demands = DemandGenerator.genRandomDemands(g, 100);
			DemandIO.writeRDPDemands("demands/real/" + g.getName() + ".dem", g, demands);
		}
	}

}
