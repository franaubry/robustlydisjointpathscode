package scripts;

import java.io.File;
import java.util.ArrayList;

import dataStructures.RDPDemand;
import graphs.Graph;
import graphs.MaxEdgeDisjointPaths;
import io.DemandIO;
import io.GraphIO;

public class CheckDemandMinCuts {

	public static void main(String[] args) {
		String[] dirs = new String[] {"zoo_large", "real", "rf"};
		for(String d : dirs) {
			File f = new File("topologies/" + d + "/");
			for(String instancefn : f.list()) {
				Graph g = GraphIO.read("topologies/" + d + "/" + instancefn);
				ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, "demands/" + d + "/" + g.getName() + ".dem");
				for(RDPDemand demand : demands) {
					MaxEdgeDisjointPaths medg = new MaxEdgeDisjointPaths(g, new int[] {demand.source1(),  demand.source2()}, new int[] {demand.dest1(), demand.dest2()});
					if(medg.getMaxFlow() < 2) {
						System.out.println(g.getName());
						System.exit(0);
					}
				}
			}
		}
		System.out.println("done");
	}

}
