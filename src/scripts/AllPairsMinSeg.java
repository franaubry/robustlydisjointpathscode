package scripts;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import dataStructures.Count;
import dataStructures.RDPDemand;
import graphs.Dijkstra;
import graphs.Edge;
import graphs.Graph;
import graphs.MaxEdgeDisjointPaths;
import graphs.Path;
import io.DemandIO;
import io.GraphIO;
import sr.Segmenter;

public class AllPairsMinSeg {

	public static void main(String[] args) throws IOException {
		//String[] dirs = new String[] {"OVH", "real"};
		//String[] dirs = new String[] {"zoo_large"};
		String[] dirs = new String[] {"OVH"};
		Count<Integer> cnt = new Count<>();
		for(String d : dirs) {
			File f = new File("topologies/" + d + "/");
			for(String instancefn : f.list()) {
				System.out.println(instancefn);
				Graph g = GraphIO.read("topologies/" + d + "/" + instancefn);
				Segmenter seg = new Segmenter(g);
				double[] lat = g.getLatencies();
				for(int u = 0; u < g.V(); u++) {
					Edge[] parent = Dijkstra.shortestPathTree(g, u, lat);
					for(int v = 0; v < g.V(); v++) {
						if(u == v) continue;
						Path path = Dijkstra.buildPath(parent, u, v);
						if(path != null) {
							int nbseg = seg.computSegWeight(path);
							cnt.add(nbseg);							
						} else  {
							System.out.println("null");
						}
					}
				}
			}
		}
		PrintWriter writer = new PrintWriter(new FileWriter("results/minseg/all.txt"));
		double total = 0;
		for(int i = 0; i < 100; i++) {
			if(cnt.count(i) > 0) {
				double percent = (100 * (double)cnt.count(i) / cnt.total());
				total += percent;
				writer.write(i + " " + percent + "\n");
				System.out.println(i + " " + percent);
			}
		}
		writer.close();
		System.out.println(total);
	}

}
