package scripts;

import java.util.ArrayList;
import java.util.BitSet;

import dataStructures.RDPDemand;
import graphs.Graph;
import io.DemandIO;
import io.GraphIO;
import rdp.FailureGen;
import rdp.RDPOneSidedFailures;
import rdp.RDPSolution;
import rdp.WeaklyRDP;

public class CheckWeaklyVSRDPSingleLink {
	
	public static void main(String[] args) {
		String topologyfn = args[0];
		String demandfn = args[1];
		int maxseg = Integer.parseInt(args[2]);
		Graph g = GraphIO.read(topologyfn);
		ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, demandfn);
		BitSet[] F = FailureGen.genAllEdges(g);
		
		WeaklyRDP wrdp = new WeaklyRDP(g, F);
		RDPOneSidedFailures rdp1 = new RDPOneSidedFailures(g, F);
		
		for(RDPDemand demand : demands) {
			System.out.println(demand);
			RDPSolution wsol = wrdp.findPaths(demand, maxseg);
			
			RDPSolution sol1 = rdp1.findPaths(demand, maxseg);
			
			if(wsol.rdpExist() != sol1.rdpExist()) {
				System.out.println("solution existence inconsistency");
				System.out.println("wsol has sol: " + wsol.rdpExist());
				System.out.println("sol1 has sol: " + sol1.rdpExist());
			}
			
			double wlat = wsol.maxLat();
			double lat1 = sol1.maxLat();
			
			if(Math.abs(wlat - lat1) > 1e-6) {
				System.out.println("latency inconsistency");
				System.out.println("wsol has lat: " + wlat);
				System.out.println("sol1 has lat: " + lat1);
			}
				
		}
		System.out.println("done");
	}

}
