package scripts;

import java.io.File;

import generation.DemandGenerator;
import graphs.Graph;
import io.DemandIO;
import io.GraphIO;

public class GenerateZooDemands {
	
	public static void main(String[] args) {
		File dir = new File("topologies/zoo_large/");
		for(String fn : dir.list()) {
			Graph g = GraphIO.read("topologies/zoo_large/" + fn);
			System.out.println("generating random demands for: " + fn);
			DemandIO.writeRDPDemands("demands/zoo_large/" + g.getName() + ".dem", g, DemandGenerator.genRandomDemands(g, 500));
		}
	}

}
