package dataStructures;

import java.util.Arrays;
import java.util.Stack;

public class SparseSet {

	private int[] index;
	private int[] value;
	private int split;
	private Stack<Integer> restoreStack;
	
	public SparseSet(int n) {
		value = new int[n];
		index = new int[n];
		for(int i = 0; i < n; i++) {
			value[i] = index[i] = i;
		}
		split = n;
		restoreStack = new Stack<>();
	}
	
	public int size() {
		return split;
	}
	
	public void addRestorePoint() {
		restoreStack.push(0);
	}
	
	public void remove(int v) {
		if(contains(v)) {
			if(!restoreStack.isEmpty()) {
				restoreStack.push(restoreStack.pop() + 1);				
			}
			split--;
			moveToSplit(v);			
		}
	}
	
	public boolean contains(int v) {
		return index[v] < split;
	}
	
	public void add(int v) {
		if(!contains(v)) {
			moveToSplit(v);
			split++;
		}
	}
	
	public int get(int i) {
		return value[i];
	}
	
	/*
	 * Restores this set as it was prior to the last batch of
	 * remove() operations.
	 */
	public void restore() {
		split += restoreStack.pop();
	}
	
	private void moveToSplit(int v) {
		int i = index[v];
		int tmp = value[split];
		value[split] = v;
		index[v] = split;
		value[i] = tmp;
		index[tmp] = i;	
	}

	public String toString2() {
		int[] tmp = Arrays.copyOf(value, size());
		Arrays.sort(tmp);
		return Arrays.toString(tmp).replace("[", "{").replace("]", "}");
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < split; i++) {
			sb.append(value[i]);
			if(i < split - 1) {
				sb.append(", ");
			}
		}
		sb.append(" | ");
		for(int i = split; i < value.length; i++) {
			sb.append(value[i]);
			if(i < value.length - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
}
