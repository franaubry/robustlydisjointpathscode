package dataStructures;

/**
 * Class that represents an ordered pair of integers.
 * 
 * @author f.aubry@uclouvain.be
 */
public class IntTriple {
	
	public int x, y, z;
	
	public IntTriple(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public boolean equals(Object other) {
		if(other instanceof IntTriple) {
			IntTriple o = (IntTriple)other;
			return x == o.x && y == o.y && z == o.z;
		}
		return false;
	}
	
	public int hashCode() {
		return 17 * x + 31 * y + 31 * 31 * z;
	}
	
	public String toString() {
		return String.format("(%d, %d, %d)", x, y, z);
	}

}
