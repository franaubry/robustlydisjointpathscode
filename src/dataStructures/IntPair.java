package dataStructures;

/**
 * Class that represents an ordered pair of integers.
 * 
 * @author f.aubry@uclouvain.be
 */
public class IntPair {
	
	public int x, y;
	
	public IntPair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Object other) {
		if(other instanceof IntPair) {
			IntPair o = (IntPair)other;
			return x == o.x && y == o.y;
		}
		return false;
	}
	
	public int hashCode() {
		return 17 * x + 31 * y;
	}
	
	public String toString() {
		return String.format("(%d, %d)", x, y);
	}

}
