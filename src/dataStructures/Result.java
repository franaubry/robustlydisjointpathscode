package dataStructures;

import graphs.Graph;
import graphs.Suurballe;
import rdp.RDPSolution;

public class Result {
	
	public Graph g;
	public RDPSolution sol;
	public RDPDemand demand;
	public long durationRDP, durationSuurballe, mincut;
	public Suurballe suurballe;
	public double nomlat1, nomlat2;
	
	public Result(RDPSolution sol, Graph g, RDPDemand demand, long durationRDP, Suurballe suurballe, long durationSuurballe, int mincut, double nomlat1, double nomlat2) {
		this.sol = sol;
		this.g = g;
		this.demand = demand;
		this.durationRDP = durationRDP;
		this.suurballe = suurballe;
		this.durationSuurballe = durationSuurballe;
		this.mincut = mincut;
		this.nomlat1 = nomlat1;
		this.nomlat2 = nomlat2;
	}
	
}