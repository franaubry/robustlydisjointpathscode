package dataStructures;

import java.util.HashMap;
import java.util.Set;

public class Count<E> {
	
	private HashMap<E, Integer> cnt;
	private int total;
	
	public Count() {
		cnt = new HashMap<>();
		total = 0;
	}
	
	public int count(E e) {
		Integer c = cnt.get(e);
		if(c == null) return 0;
		return c;
	}
	
	public void add(E e) {
		int c = count(e);
		cnt.put(e, c + 1);
		total += 1;
	}
	
	public void remove(E e) {
		int c = count(e);
		if(c - 1 <= 0) {
			cnt.remove(e);
		} else {
			cnt.put(e, c - 1);
		}
		total -= 1;
	}
	
	public int total() {
		return total;
	}
	
	public Set<E> keys() {
		return cnt.keySet();
	}
	
	public String toString() {
		return cnt.toString();
	}

}
