package dataStructures;

import graphs.Graph;

public class RDPDemand {
	
	private int s1, s2, d1, d2;
	
	public RDPDemand(int source1, int source2, int dest1, int dest2) {
		this.s1 = source1;
		this.s2 = source2;
		this.d1 = dest1;
		this.d2 = dest2;
	}
	
	public RDPDemand(Graph g, String source1, String source2, String dest1, String dest2) {
		this.s1 = g.getIndex(source1);
		this.s2 = g.getIndex(source2);
		this.d1 = g.getIndex(dest1);
		this.d2 = g.getIndex(dest2);
	}

	public int source1() {
		return s1;
	}
	
	public int source2() {
		return s2;
	}
	
	public int dest1() {
		return d1;
	}
	
	public int dest2() {
		return d2;
	}
	
	public boolean isDest(int v) {
		return d1 == v || d2 == v;
	}
	
	public boolean isSource(int v) {
		return s1 == v || s2 == v;
	}
	
	public int hashCode() {
		return s1 + 31 * s2 + 31 * 31 * d1 + 31 * 31 * 31 * d2;
	}
	
	public boolean equals(Object other) {
		if(other instanceof RDPDemand) {
			RDPDemand o = (RDPDemand)other;
			return s1 == o.s1 && s2 == o.s2 && d1 == o.d1 && d2 == o.d2;
		}
		return false;
	}
	
	public String fileStr(Graph g) {
		return String.format("%s %s %s %s", g.getLabel(s1), g.getLabel(d1), g.getLabel(s2), g.getLabel(d2));
	}

	public String toString(Graph g) {
		return String.format("[%s->%s,%s->%s]", g.getLabel(s1), g.getLabel(d1), g.getLabel(s2), g.getLabel(d2));
	}
	
	public String toString() {
		return String.format("[%d->%d,%d->%d]", s1, d1, s2, d2);
	}

}
