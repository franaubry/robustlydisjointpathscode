package sr;

import graphs.AllPairsShortestPaths;
import graphs.Edge;
import graphs.Graph;
import graphs.Path;

public class Segmenter {

	private Graph g;
	private int[][] igp;
	private double[][] dist;

	public Segmenter(Graph g) {
		this.g = g;
		igp = new int[g.V()][g.V()];
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				igp[e.orig()][e.dest()] = e.igp();
			}
		}
		dist = AllPairsShortestPaths.allPairsMinIGP(g);
	}

	public int computSegWeight(Path path) {
		int root = path.get(0);
		int segW = 0;
		for(int i = 0; i < path.size() - 1; i++) {
			int v = path.get(i);
			int u = path.get(i + 1);
			if(!edgeInDag(root, v, u) || dagInDeg(root, u) > 1) {
				if(dagInDeg(v, u) == 1) {
					segW += 1;
					root = v;
				} else {
					segW += 2;
					root = u;
				}
			}
		}
		if(root != path.last()) {
			segW += 1;
		}
		return segW;
	}

	private int dagInDeg(int root, int u) {
		int indeg = 0;
		for(Edge e : g.inEdges(u)) {
			if(edgeInDag(root, e.orig(), u)) indeg += 1;
		}
		return indeg;
	}

	private boolean edgeInDag(int root, int u, int v) {
		return dist[root][u] + igp[u][v] == dist[root][v];
	}

}
