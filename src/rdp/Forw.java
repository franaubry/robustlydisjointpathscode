package rdp;

import java.util.BitSet;

import graphs.Dijkstra;
import graphs.Edge;
import graphs.Graph;
import graphs.Path;
import graphs.TopologicalSort;

public class Forw {

	private Graph g;
	private Graph[] spdags;
	private BitSet[][] forw;
	private double[][] forwLat;
	
	public Forw(Graph g) {
		this.g = g;
		spdags = new Graph[g.V()];
		for(int v = 0; v < g.V(); v++) {
			spdags[v] = Dijkstra.forwDAG(g, v);
		}
		computeForw();
	}
	
	public BitSet forw(Path p) {
		BitSet fwp = new BitSet();
		for(int i = 1; i < p.size(); i++) {
			int x = p.get(i - 1);
			int y = p.get(i);
			fwp.or(forw[x][y]);
		}
		return fwp;
	}
	
	
	public boolean forwOk(int x, int y) {
		return forw[x][y].cardinality() != 0;
	}
	
		
	public double forwLat(int x, int y) {
		return forwLat[x][y];
	}
	

	private void computeForw() {
		// initialize the empty forwarding graphs
		forw = new BitSet[g.V()][g.V()];
		forwLat = new double[g.V()][g.V()];
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				forw[i][j] = new BitSet();
				forwLat[i][j] = Double.NEGATIVE_INFINITY;
			}
		}
		// compute the forwarding graphs based on the topological orders
		for(int x = 0; x < g.V(); x++) {
			int[] order = new TopologicalSort(spdags[x]).getOrder();
			forwLat[x][x] = 0;
			for(int i = 1; i < g.V(); i++) {
				int xi = order[i];
				for(Edge e : spdags[x].inEdges(xi)) {
					forw[x][xi].or(forw[x][e.orig()]);
					forw[x][xi].set(e.index());
					forw[x][xi].set(e.getReverse().index());
					forwLat[x][xi] = Math.max(forwLat[x][xi], forwLat[x][e.orig()] + e.lat());
				}
			}
		}
	}

	public BitSet forw(int x, int y) {
		return forw[x][y];
	}

}
