package rdp;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;

import dataStructures.IntPair;
import graphs.Edge;
import graphs.Graph;

public class FailureGen {
	
	public static BitSet[] genAllEdges(Graph g) {
		BitSet[] F = new BitSet[g.E() / 2];
		BitSet done = new BitSet();
		int i = 0;
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				if(!done.get(e.index())) {
					F[i] = new BitSet();
					F[i].set(e.index());
					F[i].set(e.getReverse().index());
					done.or(F[i]);
					i++;
				}
			}
		}
		return F;
	}
	
	public static BitSet[] random3EdgeFailures(Graph g, int nbSets) {
		BitSet[] F = new BitSet[nbSets];
		Random rnd = new Random(31);
		for(int i = 0; i < nbSets; i++) {
			F[i] = new BitSet();
			for(int j = 0; j < 3; j++) {
				int idx = rnd.nextInt(g.E());
				Edge e = g.getEdge(idx);
				F[i].set(e.index());
				F[i].set(e.getReverse().index());
			}
		}
		return F;
	}

	public static BitSet[] randomEdgeFailures(Graph g, int nbSets, int size) {
		BitSet[] F = new BitSet[nbSets];
		Random rnd = new Random(31);
		for(int i = 0; i < nbSets; i++) {
			F[i] = new BitSet();
			for(int j = 0; j < size; j++) {
				int idx = rnd.nextInt(g.E());
				Edge e = g.getEdge(idx);
				F[i].set(e.index());
				F[i].set(e.getReverse().index());
			}
		}
		return F;
	}
	
	public static BitSet[] all2Edges(Graph g) {
		ArrayList<BitSet> F = new ArrayList<>();
		HashSet<IntPair> done = new HashSet<>();
		Edge[] edges = g.getEdgesById();
		for(int i = 0; i < edges.length; i++) {
			for(int j = 0; j < edges.length; j++) {
				if(i == j) continue;
				int idx1 = Math.min(edges[i].index(), edges[i].getReverse().index());
				int idx2 = Math.min(edges[j].index(), edges[j].getReverse().index());
				if(!done.contains(new IntPair(idx1, idx2))) {
					done.add(new IntPair(idx1, idx2));
					BitSet f = new BitSet();
					f.set(edges[i].index());
					f.set(edges[i].getReverse().index());
					f.set(edges[j].index());
					f.set(edges[j].getReverse().index());
					F.add(f);
				}
			}
		}
		BitSet[] ret = new BitSet[F.size()];
		for(int i = 0; i < F.size(); i++) {
			ret[i] = F.get(i);
		}
		System.out.println(ret.length + " " + (g.E() * g.E() / 4));
		return ret;
	}

}
