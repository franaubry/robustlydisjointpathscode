package rdp;

import java.util.BitSet;

import graphs.Edge;

public class FailureSet {
	
	private BitSet f;
	
	public FailureSet() {
		f = new BitSet();
	}
	
	public void add(Edge e) {
		f.set(e.index());
		f.set(e.getReverse().index());
	}
	
	public BitSet bitset() {
		return f;
	}
	
	public String toString() {
		return f.toString();
	}

}
