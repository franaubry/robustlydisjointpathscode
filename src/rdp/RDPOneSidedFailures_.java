package rdp;

import java.util.Arrays;
import java.util.BitSet;

import graphs.AllPairsShortestPaths;
import graphs.Graph;
import graphs.Path;

/**
 * Class used to efficiently compute pairs of robustly disjoint paths.
 * 
 * @author f.aubry@uclouvain.be
 *
 */
public class RDPOneSidedFailures_ extends RDPComputer {

	private Graph g;
	private ForwFailOneSidedFailures ffs;
	private double[][] latDist;
	private int s1, s2, t1, t2;
	private Path path1, path2;
	private double lat1, lat2;
	private BitSet forw1, fail1, forw2, fail2;
	private boolean solutionFound;
	
	/*
	 * Create an instance of our algorithm to compute robustly disjoint paths.
	 * 
	 * g: is the graph representing the network topology
	 * 
	 * optimizeLat: should be set to true if we can to minimize the maximum latency of the
	 * two paths. otherwise the algorithm will stop as soon as a feasible solution is found.
	 * 
	 * precomputeData: if set to true then the algorithm will precompute all the data that is
	 * independent from the demands only once (for instance, the forwarding graphs). Otherwise it
	 * will recompute it every for each demand. This is used for the experiments to be able to take
	 * into account this computation time into the demand computation time.
	 * 
	 * verbose: whether or not the algorithm prints some information about its current state.
	 */
	public RDPOneSidedFailures_(Graph g, BitSet[] F) {
		this.g = g;
		ffs = new ForwFailOneSidedFailures(g, F);
		latDist = AllPairsShortestPaths.allPairsMinLat(g);
	}

	/*
	 * Get the last s1 for which findPaths was called.
	 */
	public int s1() {
		return s1;
	}

	/*
	 * Get the last s2 for which findPaths was called.
	 */
	public int s2() {
		return s2;
	}

	/*
	 * Get the last t1 for which findPaths was called.
	 */
	public int t1() {
		return t1;
	}

	/*
	 * Get the last t2 for which findPaths was called.
	 */
	public int t2() {
		return t2;
	}

	/*
	 * Find robustly-disjoint path between s1 and t1, s2 and t2 with
	 * at most maxSize segments. maxSize includes the segments for 
	 * s1, s2, t1 and t2 so it should always be >= 2.
	 */
	public RDPSolution findPaths(int s1, int s2, int t1, int t2, int maxSize) {
		this.s1 = s1;
		this.s2 = s2;
		this.t1 = t1;
		this.t2 = t2;
		// initialize the paths
		path1 = path2 = null;
		// initialize the latencies
		lat1 = lat2 = Double.POSITIVE_INFINITY;
		// set that solution has been found so far
		solutionFound = false;
		// initialize the pivot path for the search as an empty path starting at s1
		Path p1 = new Path();
		p1.add(s1);
		// initialize empty forwarding and failure subgraphs for the pivot path
		BitSet fw1 = new BitSet();
		BitSet fa1 = new BitSet();
		// iterate on the path size to perform iterative deepening
		for(int size = maxSize; size <= maxSize; size++) {
			// find the best secondary path compatible with the current pivot path (currently empty)
			Path p2 = findSecondaryPath(p1, fw1, fa1, size);
			if(p2 == null) {
				return new RDPSolution(null, null);		
			}
			// compute the forw and fail subgraph for the secondary path
			BitSet fw2 = new BitSet();
			BitSet fa2 = new BitSet();
			for(int i = 1; i < p2.size(); i++) {
				fw2.or(ffs.forw(p2.get(i - 1), p2.get(i)));
				fa2.or(ffs.fail(p2.get(i - 1), p2.get(i)));
			}
			// call a depth-first search limited at depth size
			findPaths(p1, fw1, fa1, 0, p2, fw2, fa2, p2.getLat(), size);
		}
		if(solutionFound) {
			// a solution has been found, return it
			return new RDPSolution(path1, path2);
		}
		return new RDPSolution();
	}

	/*
	 * Perform a limited depth first search to find robustly-disjoint paths.
	 */
	private void findPaths(Path p1, BitSet fw1, BitSet fa1, double lat1, Path p2, BitSet fw2, BitSet fa2, double lat2, int seg) {
		// check if we can cutoff the search by latency
		if(cutOff(p1, p2, lat1, lat2)) {
			return;
		}
		if(p1.last() == t1) {
			// the pivot path is complete, since it was not cutoff
			// it must be better than the previous one
			// set the new paths as the solution so far
			path1 = p1.copy();
			path2 = p2.copy();
			this.lat1 = lat1;
			this.lat2 = lat2;
			forw1 = new BitSet();
			forw1.or(fw1);
			fail1 = new BitSet();
			fail1.or(fa1);
			forw2 = new BitSet();
			forw2.or(fw2);
			fail2 = new BitSet();
			fail2.or(fa2);
			solutionFound = true;
			return;
		}
		int last = p1.last();
		// iterate over the possible extensions of the pivot path
		for(int next = 0; next < g.V(); next++) {
			// ignore irrelevant nodes
			if(p1.contains(next) || (p1.size() == seg - 1 && next != t1) || !ffs.forwOk(last, next) || !ffs.failOk(last, next)) continue;
			// extend the pivot path
			p1.add(next);
			// update forw and fail for the pivot path
			BitSet fw1c = new BitSet();
			fw1c.or(fw1);
			fw1c.or(ffs.forw(last, next));
			BitSet fa1c = new BitSet();
			fa1c.or(fa1);
			fa1c.or(ffs.fail(last, next));
			if(!fw1c.intersects(fa2) && !fa1c.intersects(fw2)) {
				// the new pivot is still compatible with the secondary path, continue
				findPaths(p1, fw1c, fa1c, lat1 + ffs.forwLat(last, next), p2, fw2, fa2, lat2, seg);
			} else {
				// the new pivot is not compatible with the secondary path, build a new one
				Path p2c = findSecondaryPath(p1, fw1c, fa1c, seg);
				if(p2c != null) {
					// we found a compatible secondary path, continue the search
					BitSet fw2c = new BitSet();
					BitSet fa2c = new BitSet();
					for(int i = 1; i < p2c.size(); i++) {
						fw2c.or(ffs.forw(p2c.get(i - 1), p2c.get(i)));
						fa2c.or(ffs.fail(p2c.get(i - 1), p2c.get(i)));
					}
					findPaths(p1, fw1c, fa1c, lat1 + ffs.forwLat(last, next), p2c, fw2c, fa2c, p2c.getLat(), seg);
				}
			}
			// backtrack the last decision for the pivot path
			p1.removeLast();
		}
	}

	/*
	 * Check whether we can cutoff the search from the latency.
	 */
	private boolean cutOff(Path p1, Path p2, double lat1, double lat2) {
		//return Math.max(lat1, lat2) >= Math.max(this.lat1, this.lat2);
		return Math.max(lat1 + latDist[p1.last()][t1], lat2) >= Math.max(this.lat1, this.lat2);
	}

	/*
	 * Given the pivot path p1 and a maximum number of segments seg, compute
	 * the minimum latency secondary path that is robustly-disjoint from
	 * p1.
	 */
	private Path findSecondaryPath(Path p1, BitSet fw1, BitSet fa1, int seg) {
		// initialize the distances. dist[i][x] = min latency of a path that is robustly disjoint
		// with p1, used most i segments and ends at node x
		double[][] dist = new double[seg][g.V()];
		for(int i = 0; i < dist.length; i++) Arrays.fill(dist[i], Double.POSITIVE_INFINITY);
		dist[0][s2] = 0;
		// initialize the parents to rebuild the solution
		int[][] parent = new int[seg + 1][g.V()];
		for(int i = 0; i < parent.length; i++) Arrays.fill(parent[i], -1);
		// loop of i and x to compute to fill in dist
		for(int i = 1; i < seg; i++) {
			for(int x = 0; x < g.V(); x++) {
				// compute dist[i][x] = min[y in V] dist[i - 1][y] + forwLat[y][x]
				dist[i][x] = dist[i - 1][x];
				for(int y = 0; y < g.V(); y++) {
					if(x == y) continue;
					if(!ffs.forwOk(y, x) || !ffs.failOk(y, x)) continue;
					if(!ffs.forw(y, x).intersects(fa1) && !ffs.fail(y, x).intersects(fw1)) {
						if(dist[i - 1][y] + ffs.forwLat(y, x) < dist[i][x]) {
							dist[i][x] = dist[i - 1][y] + ffs.forwLat(y, x);
							parent[i][x] = y;
						}
					}
				}
			}
		}
		// check whether a solution was found
		if(dist[seg - 1][t2] == Double.POSITIVE_INFINITY) return null;
		// solution exists, build it
		double best = Double.POSITIVE_INFINITY;
		int besti = -1;
		for(int i = 0; i < seg; i++) {
			if(best > dist[i][t2]) {
				best = dist[i][t2];
				besti = i;
			}
		}
		Path p = buildPath(parent, s2, t2, besti);
		p.setLat(best);
		return p;
	}

	/*
	 * Auxiliary function to build the secondary path.
	 */
	private Path buildPath(int[][] parent, int s, int t, int seg) {
		Path p2 = new Path();
		p2.add(t);
		while(parent[seg][t] != -1) {
			t = parent[seg][t];
			seg--;
			p2.add(t);
		}
		p2.reverse();
		return p2;
	}

}
