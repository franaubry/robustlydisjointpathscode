package rdp;

import java.util.Arrays;
import java.util.BitSet;

import graphs.Dijkstra;
import graphs.Edge;
import graphs.Graph;
import graphs.Path;
import graphs.TopologicalSort;

public class ForwFailOneSidedFailures {

	private Graph g;
	private Graph[] spdags;
	private BitSet[] F;
	private BitSet[][] fail, forw;
	private BitSet[][] forwFInter;
	private double[][] forwLat;
	private boolean[][] failOk;
	
	public ForwFailOneSidedFailures(Graph g, BitSet[] F) {
		this.g = g;
		this.F = F;
		spdags = new Graph[g.V()];
		for(int v = 0; v < g.V(); v++) {
			spdags[v] = Dijkstra.forwDAG(g, v);
		}
		failOk = new boolean[g.V()][g.V()];
		for(int x = 0; x < g.V(); x++) {
			Arrays.fill(failOk[x], true);
		}
		computeForw();
		computeFail();
	}
	
	public BitSet forw(Path p) {
		BitSet fwp = new BitSet();
		for(int i = 1; i < p.size(); i++) {
			int x = p.get(i - 1);
			int y = p.get(i);
			fwp.or(forw[x][y]);
		}
		return fwp;
	}
	
	public BitSet fail(Path p) {
		BitSet fap = new BitSet();
		for(int i = 1; i < p.size(); i++) {
			fap.or(fail[p.get(i - 1)][p.get(i)]);
		}
		return fap;
	}
	
	public BitSet FInter(int x, int y) {
		return forwFInter[x][y];
	}
	
	public boolean forwOk(int x, int y) {
		return forw[x][y].cardinality() != 0;
	}
	
	public boolean failOk(int x, int y) {
		return failOk[x][y];
	}
	
	private void computeFail() {
		// initialize the empty failure graphs
		fail = new BitSet[g.V()][g.V()];
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				fail[i][j] = new BitSet();
			}
		}
		// compute the failure graphs based on the topological orders
		Edge[] edges = g.getEdgesById();
		BitSet inter;
		BitSet active = g.activeEdges();
		for(int x = 0; x < g.V(); x++) {
			for(BitSet f : F) {
				inter = new BitSet();
				inter.or(spdags[x].edgeSet());
				inter.and(f);
				
				// check whether we can ignore edge f to accelerate the process	
				if(inter.cardinality() == 0) continue;
				if(inter.cardinality() == 1) {
					Edge edge = edges[inter.nextSetBit(0)];
					if(spdags[x].inDeg(edge.dest()) > 1 && spdags[x].inDeg(edge.orig()) > 1) continue;
				}
				
				// define forwxf such that forwxf[y] = forw(G \ f, x, y)
				BitSet[] forwxf = new BitSet[g.V()];
				for(int y = 0; y < g.V(); y++) {
					forwxf[y] = new BitSet();
				}
				// deactivate the edges
				active.xor(f);
				Graph spxf = Dijkstra.forwDAG(g, x);
				int[] order = new TopologicalSort(spxf).getOrder();
				for(int i = 1; i < g.V(); i++) {
					int xi = order[i];
					for(Edge e : spxf.inEdges(xi)) {
						forwxf[xi].or(forwxf[e.orig()]);
						forwxf[xi].set(e.index());
						forwxf[xi].set(e.getReverse().index());
					}
				}
				for(int y = 0; y < g.V(); y++) {
					if(forwxf[y].cardinality() == 0) {
						failOk[x][y] = false;
					}
					fail[x][y].or(forwxf[y]);
				}
				
				// reactivate the edges
				active.or(f);
			}
		}
	}
	
	public double forwLat(int x, int y) {
		return forwLat[x][y];
	}
	

	private void computeForw() {
		// initialize the empty forwarding graphs
		forw = new BitSet[g.V()][g.V()];
		forwLat = new double[g.V()][g.V()];
		forwFInter = new BitSet[g.V()][g.V()];
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				forw[i][j] = new BitSet();
				forwFInter[i][j] = new BitSet();
				forwLat[i][j] = Double.NEGATIVE_INFINITY;
			}
		}
		// compute the forwarding graphs based on the topological orders
		for(int x = 0; x < g.V(); x++) {
			int[] order = new TopologicalSort(spdags[x]).getOrder();
			forwLat[x][x] = 0;
			for(int i = 1; i < g.V(); i++) {
				int xi = order[i];
				for(Edge e : spdags[x].inEdges(xi)) {
					forw[x][xi].or(forw[x][e.orig()]);
					forw[x][xi].set(e.index());
					forw[x][xi].set(e.getReverse().index());
					forwLat[x][xi] = Math.max(forwLat[x][xi], forwLat[x][e.orig()] + e.lat());
				}
			}
		}
		for(int x = 0; x < g.V(); x++) {
			for(int y = 0; y < g.V(); y++) {
				for(int i = 0; i < F.length; i++) {
					if(F[i].intersects(forw[x][y])) {
						forwFInter[x][y].set(i);
					}
				}
			}
		}
	}

	public BitSet forw(int x, int y) {
		return forw[x][y];
	}

	public BitSet fail(int x, int y) {
		return fail[x][y];
	}

}
