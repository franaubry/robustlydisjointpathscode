package rdp;

import graphs.Path;

public class RDPSolution {

	private Path p1, p2;
	
	public RDPSolution(Path p1, Path p2) {
		this.p1 = p1.copy();
		this.p2 = p2.copy();
	}
	
	public RDPSolution() {
		this.p1 = this.p2 = null;
	}
	
	public Path getP1() {
		return p1;
	}
	
	public Path getP2() {
		return p2;
	}
	
	public double maxLat() {
		return Math.max(p1.getLat(), p2.getLat());
	}
	
	public double minLat() {
		return Math.min(p1.getLat(), p2.getLat());
	}
	
	public boolean rdpExist() {
		return p1 != null;
	}
	
}
