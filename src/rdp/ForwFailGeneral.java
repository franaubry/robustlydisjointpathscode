package rdp;

import java.util.BitSet;
import graphs.Dijkstra;
import graphs.Edge;
import graphs.Graph;
import graphs.Path;
import graphs.TopologicalSort;

public class ForwFailGeneral {

	private Graph g;
	private BitSet[] F;
	private Graph[] spdags;
	private BitSet[][] forw;
	private double[][] forwLat;
	private BitSet[][][] fail;
	
	public ForwFailGeneral(Graph g, BitSet[] F) {
		this.g = g;
		this.F = F;
		spdags = new Graph[g.V()];
		for(int v = 0; v < g.V(); v++) {
			spdags[v] = Dijkstra.forwDAG(g, v);
		}
		computeForw();
		computeFail();
	}
	
	public int getFSize() {
		return F.length;
	}
	
	public Graph getGraph() {
		return g;
	}

	private void computeForw() {
		// initialize the empty forwarding graphs
		forw = new BitSet[g.V()][g.V()];
		forwLat = new double[g.V()][g.V()];
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				forw[i][j] = new BitSet();
				forwLat[i][j] = Double.NEGATIVE_INFINITY;
			}
		}
		// compute the forwarding graphs based on the topological orders
		for(int x = 0; x < g.V(); x++) {
			int[] order = new TopologicalSort(spdags[x]).getOrder();
			forwLat[x][x] = 0;
			for(int i = 1; i < g.V(); i++) {
				int xi = order[i];
				for(Edge e : spdags[x].inEdges(xi)) {
					forw[x][xi].or(forw[x][e.orig()]);
					forw[x][xi].set(e.index());
					forw[x][xi].set(e.getReverse().index());
					forwLat[x][xi] = Math.max(forwLat[x][xi], forwLat[x][e.orig()] + e.lat());
				}
			}
		}
	}
	
	private void computeFail() {
		// initialize the empty failure graps
		fail = new BitSet[g.V()][g.V()][F.length];
		for(int x = 0; x < g.V(); x++) {
			for(int y = 0; y < g.V(); y++) {
				for(int f = 0; f < F.length; f++) {
					fail[x][y][f] = new BitSet();
				}
			}
		}
		// compute the failure graphs based on the topological orders
		// set every edge to be active
		BitSet active = g.activeEdges();
		// initialize Dijkstra's algorithm
		for(int f = 0; f < F.length; f++) {
			// deactivate all edges from failure set f
			active.xor(F[f]);
			// compute forw(x, y, f) for all x and y
			for(int x = 0; x < g.V(); x++) {				
				Graph dagxf = Dijkstra.forwDAG(g, x);
				int[] order = new TopologicalSort(dagxf).getOrder();
				for(int i = 1; i < g.V(); i++) {
					int xi = order[i];
					for(Edge e : dagxf.inEdges(xi)) {
						fail[x][xi][f].or(fail[x][e.orig()][f]);
						fail[x][xi][f].set(e.index());
						fail[x][xi][f].set(e.getReverse().index());
					}
				}
			}
			// reactivate all edge from failure set f
			active.or(F[f]);
		}
	}
	
	public BitSet forw(Path path) {
		BitSet fw = new BitSet();
		for(int i = 1; i < path.size(); i++) {
			fw.or(forw(path.get(i - 1), path.get(i)));
		}
		return fw;
	}
	
	public BitSet fail(Path path, int f) {
		BitSet fa = new BitSet();
		for(int i = 1; i < path.size(); i++) {
			fa.or(fail(path.get(i - 1), path.get(i), f));
		}
		return fa;
	}

	public double forwLat(int x, int y) {
		//computed1.add(new IntPair(x, y));
		return forwLat[x][y];
	}
	
	public BitSet forw(int x, int y) {
		return forw[x][y];
	}
	
	public BitSet forw(String x, String y) {
		return forw[g.getIndex(x)][g.getIndex(y)];
	}

	public BitSet fail(int x, int y, int f) {
		//computed2.add(new IntTriple(x, y, f));
		return fail[x][y][f];
	}
	
	public BitSet fail(String x, String y, int f) {
		return fail[g.getIndex(x)][g.getIndex(y)][f];
	}

}
