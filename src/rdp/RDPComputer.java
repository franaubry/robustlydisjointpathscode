package rdp;

import dataStructures.RDPDemand;
import graphs.Graph;

public abstract class RDPComputer {

	public void findPaths(Graph g, String s1, String s2, String t1, String t2, int maxSeg) {
		RDPDemand sd = new RDPDemand(g.getIndex(s1), g.getIndex(s2), g.getIndex(t1), g.getIndex(t2));
		findPaths(sd, maxSeg);
	}
	
	public RDPSolution findPaths(RDPDemand demand, int maxSeg) {
		return findPaths(demand.source1(), demand.source2(), demand.dest1(), demand.dest2(), maxSeg);
	}

	public abstract RDPSolution findPaths(int s1, int s2, int t1, int t2, int maxSeg);
	
	
}
