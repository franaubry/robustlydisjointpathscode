package rdp;

import java.util.Arrays;
import java.util.BitSet;

import dataStructures.RDPDemand;
import dataStructures.SparseSet;
import graphs.Graph;
import graphs.Path;
import graphs.AllPairsShortestPaths;
import graphs.ConflictGraph;

public class RDPGeneral extends RDPComputer {

	private Graph g;
	private BitSet[] F;
	private ForwFailGeneral ffs;
	private double[][] latDist;

	private int maxSeg;
	private Path bestP1, bestP2;
	private RDPDemand sd;
	
	public RDPGeneral(Graph g, BitSet[] F) {
		this.g = g;
		this.F = F;
		ffs = new ForwFailGeneral(g, F);
		latDist = AllPairsShortestPaths.allPairsMinLat(g);
	}
	
	public ForwFailGeneral getForwFail() {
		return ffs;
	}

	public RDPSolution findPaths(int s1, int s2, int t1, int t2, int maxSeg) {
		sd = new RDPDemand(s1, s2, t1, t2);
		initPathData(sd, maxSeg);
		// initialize the complete graph
		ConflictGraph K = new ConflictGraph(g);
		// initialize the first path
		Path p1 = new Path();
		p1.add(sd.source1());
		// initialize the second path
		Path p2 = findCompatiblePath(K);
		// initialize candidates
		SparseSet candidates = new SparseSet(g.V());
		candidates.remove(sd.source1());
		findPathsAux(p1, p2, candidates, K);
		return new RDPSolution(bestP1, bestP2);
	}
	
	public Path getP1() {
		return bestP1;
	}
	
	public Path getP2() {
		return bestP2;
	}

	private void initPathData(RDPDemand sd, int maxSeg) {
		this.sd = sd;
		this.maxSeg = maxSeg;
		bestP1 = bestP2 = null;
	}

	private void findPathsAux(Path p1, Path p2, SparseSet candidates, ConflictGraph K) {
		int last = p1.last();
		if(bestP1 != null && Math.max(p1.getLat() + latDist[p1.last()][sd.dest1()], p2.getLat()) >= Math.max(bestP1.getLat(), bestP2.getLat())) {
			// cutoff the search
			return;
		}
		if(p1.size() == maxSeg && p1.last() != sd.dest1()) {
			// we reached maximum length but not the destination
			return;
		}
		if(sd.isDest(last)) {
			// we have reached a better solution
			bestP1 = p1.copy();
			bestP2 = p2.copy();
			return;
		}
		// try all extensions of p1 by looping on nodes out of p1
		for(int j = candidates.size() - 1; j >= 0; j--) {
			candidates.addRestorePoint();
			int next = candidates.get(j);
			candidates.remove(next);
			p1.add(next);
			// filter the conflict graph
			filter(K, last, next);
			// the conflict graph now only contains valid edges
			// check whether p2 is still ok
			boolean pathok = true;
			for(int i = 1; i < p2.size(); i++) {
				int x = p2.get(i - 1);
				int y =  p2.get(i);
				if(!K.connected(x, y)) {
					pathok = false;
					break;
				}
			}
			if(pathok) {
				// p2 is still ok, continue
				findPathsAux(p1, p2, candidates, K);
			} else {
				// p2 became incompatible, find a new one
				Path p = findCompatiblePath(K);
				if(p != null) {
					// we found a compatible path, continue
					findPathsAux(p1, p, candidates, K);
				}
			}
			// backtrack
			// restore K
			K.restore();
			// restore p1
			p1.removeLast();
			// restore candidates
			candidates.restore();
		}
	}
	
	private void filter(ConflictGraph K, int u, int v) {
		K.createRestorePoint();
		for(int x = 0; x < K.V(); x++)  {
			for(int i = K.outN(x).size() - 1; i >= 0; i--) {
				int y = K.outN(x).get(i);
				// check whether edge (x, y) is in conflict with (u, v)
				if(ffs.forw(u, v).intersects(ffs.forw(x, y))) {
					// forwarding graphs intersect, remove (x, y) from K
					K.disconnect(x, y);
				} else {
					// forwarding graphs don't intersect, check failures
					for(int f = 0; f < F.length; f++) {
						if(ffs.fail(u, v, f).intersects(ffs.fail(x, y, f))) {
							// failure subgraphs intersect for failure f
							K.disconnect(x, y);
							break;
						}
					}
				}
			}
		}
	}

	private Path findCompatiblePath(ConflictGraph K) {
		// initialize the distances. dist[i][x] = min latency of a path that is robustly disjoint
		// with p1, used most i segments and ends at node x
		double[][] dist = new double[maxSeg][K.V()];
		for(int i = 0; i < dist.length; i++) Arrays.fill(dist[i], Double.POSITIVE_INFINITY);
		dist[0][sd.source2()] = 0;
		// initialize the parents to rebuild the solution
		int[][] parent = new int[maxSeg + 1][K.V()];
		for(int i = 0; i < parent.length; i++) Arrays.fill(parent[i], -1);
		// loop of i and x to compute to fill in dist
		for(int i = 1; i < maxSeg; i++) {
			for(int x = 0; x < K.V(); x++) {
				for(int j = 0; j < K.outN(x).size(); j++) {
					int y = K.outN(x).get(j);
					if(dist[i - 1][y] + ffs.forwLat(y, x) < dist[i][x]) {
						dist[i][x] = dist[i - 1][y] + ffs.forwLat(y, x);
						parent[i][x] = y;
					}
				}
			}
		}
		// check whether a solution was found
		if(dist[maxSeg - 1][sd.dest2()] == Double.POSITIVE_INFINITY) return null;
		// solution exists, build it
		double best = Double.POSITIVE_INFINITY;
		int besti = -1;
		for(int i = 0; i < maxSeg; i++) {
			if(best > dist[i][sd.dest2()]) {
				best = dist[i][sd.dest2()];
				besti = i;
			}
		}
		Path p = buildPath(parent, sd.source2(), sd.dest2(), besti);
		p.setLat(best);
		return p;
	}

	/*
	 * Auxiliary function to build the secondary path.
	 */
	private Path buildPath(int[][] parent, int s, int t, int seg) {
		Path p2 = new Path();
		p2.add(t);
		while(parent[seg][t] != -1) {
			t = parent[seg][t];
			seg--;
			p2.add(t);
		}
		p2.reverse();
		return p2;
	}

}
