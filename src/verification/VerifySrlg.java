package verification;

import java.util.BitSet;

import graphs.Graph;
import graphs.Path;
import io.GraphIO;
import rdp.FailureGen;

public class VerifySrlg {
	
	public static void main(String[] args) {
		String topology = args[0];
		String[] path1 = args[1].split(" ");
		String[] path2 = args[2].split(" ");
		Graph g = GraphIO.read(topology);
		Path p1 = new Path();
		for(String s : path1) p1.add(g.getIndex(s));
		Path p2 = new Path();
		for(String s : path2) p2.add(g.getIndex(s));
		BitSet[] F = FailureGen.randomEdgeFailures(g, g.E(), 5);
		boolean ok = Tools.areRobustlyDisjoint(g, p1, p2, F);
		if(!ok) {
			System.out.println("no ok");
		} else {
			System.out.println("ok");
		}
	}

}