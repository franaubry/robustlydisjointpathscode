package verification;

import java.util.BitSet;

import graphs.Graph;
import io.GraphIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;

public class CheckFFS {
	
	public static void main(String[] args) {
		Graph g = GraphIO.read("topologies/rf/1755.ntfl");
		BitSet[] single = FailureGen.genAllEdges(g);
		BitSet[] srlg = FailureGen.randomEdgeFailures(g, g.E(), 5);
		ForwFailOneSidedFailures ffsSingle = new ForwFailOneSidedFailures(g, single);
		ForwFailOneSidedFailures ffsSRLG = new ForwFailOneSidedFailures(g, srlg);
		int cnt = 0;
		int total = 0;
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				if(i == j) continue;
				BitSet tmp = new BitSet();
				tmp.or(ffsSingle.fail(i, j));
				tmp.xor(ffsSRLG.fail(i, j));
				if(tmp.cardinality() != 0) {
					System.out.println(i + " " + j);
					cnt += 1;
				}
				total += 1;
			}
		}
		System.out.println(cnt / (double)total);
		System.out.println("done");
	}

}
