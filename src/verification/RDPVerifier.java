package verification;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.LinkedList;

import graphs.Edge;
import graphs.EdgeIndexMapper;
import graphs.Graph;
import graphs.Path;
import rdp.ForwFailGeneral;
import rdp.ForwFailOneSidedFailures;

public class RDPVerifier {

	private Graph g;
	private Path p1, p2;
	private BitSet[] F;
	private ForwFailOneSidedFailures ffs;
	private BitSet forw1, forw2, fail1Union, fail2Union;
	private BitSet[] fail1;
	private BitSet[] fail2;
	private ArrayList<String> report;
	private EdgeIndexMapper eim;

	public RDPVerifier(Graph g, ForwFailOneSidedFailures ffs, Path p1, Path p2, BitSet[] F) {
		this.g = g;
		this.p1 = p1;
		this.p2 = p2;
		this.F = F;
		this.ffs = ffs;
		forw1 = ffs.forw(p1);
		forw2 = ffs.forw(p2);
		fail1Union = Tools.computeFailUnion(g, p1, F);
		fail2Union = Tools.computeFailUnion(g, p2, F);
		fail1 = new BitSet[F.length];
		fail2 = new BitSet[F.length];
		for(int i = 0; i < F.length; i++) {
			fail1[i] = Tools.computeFail(g, p1, F[i]);
			fail2[i] = Tools.computeFail(g, p2, F[i]);
		}
		eim = new EdgeIndexMapper(g);
		report = new ArrayList<>();
		check();
	}

	public boolean check() {
		boolean ok = true;
		report.add("path1:");
		report.add(p1.toString());
		report.add(p1.toString(g));
		report.add("path2:");
		report.add(p2.toString());
		report.add(p2.toString(g));
		
		BitSet tmp = ffs.forw(p1);
		tmp.xor(forw1);
		if(tmp.cardinality() != 0) {
			report.add("forw1 with tools and ffs do not match");
		}
		
		tmp = ffs.forw(p2);
		tmp.xor(forw2);
		if(tmp.cardinality() != 0) {
			report.add("forw2 with tools and ffs do not match");
		}
		
		if(forw1.intersects(forw2)) {
			ok = false;
			report.add("forw1 and forw2 intersect");
			reportIntersection(forw1, forw2);
		}
		if(fail1Union.intersects(forw2)) {
			ok = false;
			report.add("fail1Union and forw2 intersect");
			reportIntersection(fail1Union, forw2);
		}
		if(forw1.intersects(fail2Union)) {
			ok = false;
			report.add("forw1 and fail2Union intersect");
			reportIntersection(forw1, fail2Union);
		}
		for(int i = 0; i < F.length; i++) {
			if(fail1[i].intersects(fail2[i])) {
				ok = false;
				report.add(String.format("fail1[%d] and forw2[%d] intersect", i, i));
				reportIntersection(fail1[i], fail2[i]);
				report.add("edges in failure:");
				LinkedList<Edge> edges = eim.getEdges(F[i]);
				for(Edge e : edges) {
					report.add(String.format("(%d:%s, %d:%s)", e.orig(), g.getLabel(e.orig()), e.dest(), g.getLabel(e.dest())));
				}
			}
		}
		return ok;
	}

	private void reportIntersection(BitSet s1, BitSet s2) {
		BitSet inter = new BitSet();
		inter.or(s1);
		inter.and(s2);
		report.add("edges in intersection:");
		LinkedList<Edge> edges = eim.getEdges(inter);
		for(Edge e : edges) {
			report.add(String.format("(%d:%s, %d:%s)", e.orig(), g.getLabel(e.orig()), e.dest(), g.getLabel(e.dest())));
		}	
	}
	
	public void printReport() {
		for(String s : report) {
			System.out.println(s);
		}
	}

}
