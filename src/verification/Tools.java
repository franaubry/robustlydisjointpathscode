package verification;

import java.util.BitSet;
import java.util.LinkedList;

import graphs.BFS;
import graphs.Dijkstra;
import graphs.Edge;
import graphs.EdgeIndexMapper;
import graphs.Graph;
import graphs.Path;

public class Tools {
	
	public static boolean areRobustlyDisjoint(Graph g, Path p1, Path p2, BitSet[] F) {
		BitSet forw1 = computeForw(g, p1);
		BitSet forw2 = computeForw(g, p2);
		if(forw1.intersects(forw2)) return false;
		for(int i = 0; i < F.length; i++) {
			BitSet fail1 = computeFail(g, p1, F[i]);
			BitSet fail2 = computeFail(g, p2, F[i]);
			if(fail1.intersects(fail2)) return false;
		}
		return true;
	}
	
	public static boolean areRobustlyDisjointFailUnion(Graph g, Path p1, Path p2, BitSet[] F) {
		BitSet forw1 = computeForw(g, p1);
		BitSet forw2 = computeForw(g, p2);
		if(forw1.intersects(forw2)) return false;
		BitSet fail1 = computeFailUnion(g, p1, F);
		BitSet fail2 = computeFailUnion(g, p2, F);
		if(fail1.intersects(fail2)) return false;
		return true;
	}

	public static BitSet computeForw(Graph g, Path p) {
		BitSet forw = new BitSet();
		for(int i = 1; i < p.size(); i++) {
			int x = p.get(i - 1);
			int y = p.get(i);
			Graph dag = Dijkstra.forwDAG(g, x);
			dag = Dijkstra.subDag(dag, y);
			for(int u = 0; u < dag.V(); u++) {
				for(Edge e : dag.outEdges(u)) {
					forw.set(e.index());
					forw.set(e.getReverse().index());
				}
			}
		}
		return forw;
	}

	public static BitSet computeForw(Graph g, int x, int y) {
		BitSet forw = new BitSet();
		Graph dag = Dijkstra.forwDAG(g, x);
		dag = Dijkstra.subDag(dag, y);
		for(int u = 0; u < dag.V(); u++) {
			for(Edge e : dag.outEdges(u)) {
				forw.set(e.index());
				forw.set(e.getReverse().index());
			}
		}
		return forw;
	}

	public static Graph computeForwGraph(Graph g, Path p) {
		BitSet forw = computeForw(g, p);
		return computeSubgraph(g, forw);
	}

	public static BitSet computeFInter(Graph g, Path p, BitSet F[]) {
		BitSet forw = computeForw(g, p);
		BitSet finter = new BitSet();
		for(int i = 0; i < F.length; i++) {
			if(forw.intersects(F[i])) {
				finter.set(i);
			}
		}
		return finter;
	}

	public static BitSet computeFailUnion(Graph g, Path p, BitSet[] F) {
		BitSet fail = new BitSet();
		for(int i = 0; i < F.length; i++) {
			g.activeEdges().xor(F[i]);
			fail.or(computeForw(g, p));
			g.activeEdges().or(F[i]);
		}
		return fail;
	}

	public static BitSet computeFail(Graph g, Path p, BitSet f) {
		g.activeEdges().xor(f);
		BitSet fail = computeForw(g, p);
		g.activeEdges().or(f);
		return fail;
	}

	public static Graph computeFailGraph(Graph g, Path p, BitSet f) {
		BitSet fail = computeFail(g, p, f);
		return computeSubgraph(g, fail);
	}

	public static Graph computeSubgraph(Graph g, BitSet subgraphE) {
		EdgeIndexMapper eim = new EdgeIndexMapper(g);
		LinkedList<Edge> edges = eim.getEdges(subgraphE);
		Graph gf = new Graph(g.getNodeLabels());
		for(Edge e : edges) {
			gf.addEdge(e, true);
		}
		return gf;
	}

	public static boolean srPathExists(Graph g, Path p) {
		for(int i = 1; i < p.size(); i++) {
			if(!BFS.pathExists(g, p.get(i - 1), p.get(i))) return false;
		}
		return true;
	}

	public static boolean areDisjoint(Graph g, Path p1, Path p2) {
		BitSet forw1 = computeForw(g, p1);
		BitSet forw2 = computeForw(g, p2);
		return forw1.intersects(forw2);
	}

}
