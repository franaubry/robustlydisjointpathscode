package verification;

import java.util.BitSet;

import graphs.Graph;
import graphs.Path;

public class RDPInfo {
	
	private Graph g;
	private Path p1, p2;
	
	private Graph forw1, forw2;
	private Graph[] fail1, fail2;
	
	public RDPInfo(Graph g, Path p1, Path p2, BitSet[] F) {
		this.g = g;
		this.p1 = p1;
		this.p2 = p2;
		forw1 = Tools.computeForwGraph(g, p1);
		forw2 = Tools.computeForwGraph(g, p2);
		fail1 = new Graph[F.length];
		fail2 = new Graph[F.length];
		for(int i = 0; i < F.length; i++) {
			fail1[i] = Tools.computeFailGraph(g, p1, F[i]);
			fail2[i] = Tools.computeFailGraph(g, p2, F[i]);
		}	
	}
	
	public Path getP1() {
		return p1;
	}
	
	public Path getP2() {
		return p2;
	}
	
	public Graph getForw1() {
		return forw1;
	}
	
	public Graph getForw2() {
		return forw2;
	}
	
	public Graph getFail1(int i) {
		return fail1[i];
	}
	
	public Graph getFail2(int i) {
		return fail2[i];
	}

}
