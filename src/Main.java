import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.BitSet;

import dataStructures.RDPDemand;
import dataStructures.Result;
import graphs.AllPairsShortestPaths;
import graphs.Graph;
import graphs.MaxEdgeDisjointPaths;
import graphs.Suurballe;
import io.DemandIO;
import io.GraphIO;
import io.ResultIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;
import rdp.RDPOneSidedFailures;
import rdp.RDPSolution;

public class Main {

	public static ArrayList<Result> results;
	
	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		String instancefn = null;
		String demandfn = null;
		int maxSeg = -1;
		String failureType = null;
		int maxThreads = -1;
		try {
			// parse arguments
			instancefn = args[0];
			demandfn = args[1];
			maxSeg = Integer.parseInt(args[2]);
			if(maxSeg < 2) throw new IllegalArgumentException();
			failureType = args[3];
			maxThreads = Integer.parseInt(args[4]);
			if(maxThreads < 1) throw new IllegalArgumentException();
		} catch(Exception e) {
			if(args.length != 5) {
				System.out.println("usage (all arguments are mandatory):");
				System.out.println("topologyfn:  the path to the topology");
				System.out.println("demandfn:    the path to the demand file");
				System.out.println("maxSeg:      the maximum number of segments (>=2)");
				System.out.println("failureType: the id of the failures (single_link, srlg)");
				System.out.println("maxThreads:  the maximum number of threads (>=1)");
				System.exit(0);
			}	
		}
		System.out.println("init data");
		// initialize algorithm data
		Graph g = GraphIO.read(instancefn);
		BitSet[] F = null;
		switch(failureType) {
		case "single_link":
			F = FailureGen.genAllEdges(g);
			break;
		case "srlg":
			F = FailureGen.random3EdgeFailures(g, 1000);
			break;
		}
		ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, demandfn);
		long start = System.nanoTime();
		ForwFailOneSidedFailures ffs = new ForwFailOneSidedFailures(g, F);
		long end = System.nanoTime();
		double[][] latDist = AllPairsShortestPaths.allPairsMinLat(g);
		// initialize thread data
		results = new ArrayList<>();
		ArrayList<MyThread> threads = new ArrayList<>();
		for(int i = 0; i < demands.size(); i++) {
			MyThread thread = new MyThread(g, ffs, latDist, demands, i, maxSeg);
			threads.add(thread);
		}
		BitSet running = new BitSet();
		BitSet done = new BitSet();
		while(done.cardinality() < threads.size()) {
			int progress =  (int)(100.0 * done.cardinality() / demands.size());
			if(progress >= 90) break;
			System.out.println("progress: " + (int)(100.0 * done.cardinality() / demands.size()) + "%");
			for(int i = 0; i < threads.size(); i++) {
				if(!threads.get(i).isAlive() && running.cardinality() < maxThreads && !running.get(i) && !done.get(i)) {
					running.set(i);
					threads.get(i).start();
				} else if(!threads.get(i).isAlive() && running.get(i)) {
					done.set(i);
					running.clear(i);
				}
			}
			Thread.sleep(100);
		}
		
		System.out.println("writting results");
		ResultIO.writeResults(results, end - start, "results/runDemands/" + g.getName() + "_" + maxSeg + "_" + failureType + ".json");
		System.out.println("done");
	}
	
	public static class MyThread extends Thread {

		private Graph g;
		private ForwFailOneSidedFailures ffs;
		private double[][] latDist;
		private ArrayList<RDPDemand> demands;
		private int demandIndex, maxSeg;

		public MyThread(Graph g, ForwFailOneSidedFailures ffs, double[][] latDist, ArrayList<RDPDemand> demands, int demandIndex, int maxSeg) {
			this.g = g;
			this.ffs = ffs;
			this.latDist = latDist;
			this.demands = demands;
			this.demandIndex = demandIndex;
			this.maxSeg = maxSeg;
		}

		public void run() {
			RDPDemand demand = demands.get(demandIndex);
			// compute rdp
			RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, ffs, latDist);
			long start = System.nanoTime();
			RDPSolution sol = rdp.findPaths(demand, maxSeg);
			long end = System.nanoTime();
			long durationRDP = end - start;
			// compute suurballe
			start = System.nanoTime();
			Suurballe suurballe = new Suurballe(g, demand);
			end = System.nanoTime();
			long durationSuurballe = end - start;
			// compute mincut
			MaxEdgeDisjointPaths medp = new MaxEdgeDisjointPaths(g, new int[] {demand.source1(), demand.source2()}, new int[] {demand.dest1(), demand.dest2()});
			int mincut = medp.getMaxFlow();
			double nomlat1 = ffs.forwLat(demand.source1(), demand.dest1());
			double nomlat2 = ffs.forwLat(demand.source2(), demand.dest2());
			add(new Result(sol, g, demand, durationRDP, suurballe, durationSuurballe, mincut, nomlat1, nomlat2));
		}

	}

	public static synchronized void add(Result res) {
		results.add(res);
	}

}
