package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import dataStructures.RDPDemand;
import graphs.Graph;

public class DemandIO {

	public static ArrayList<RDPDemand> readRDPDemands(Graph g, String path) {
		try {
			Scanner reader = new Scanner(new FileReader(path));
			ArrayList<RDPDemand> demands = new ArrayList<>();
			while(reader.hasNextLine()) {
				String[] data = reader.nextLine().split(" ");
				String s1 = data[0];
				String s2 = data[1];
				String t1 = data[2];
				String t2 = data[3];
				demands.add(new RDPDemand(g, s1, s2, t1, t2));
			}
			reader.close();
			return demands;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void writeRDPDemands(String path, Graph g, ArrayList<RDPDemand> demands) {
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(path));
			for(RDPDemand demand : demands) {
				String s1 = g.getLabel(demand.source1());
				String s2 = g.getLabel(demand.source2());
				String t1 = g.getLabel(demand.dest1());
				String t2 = g.getLabel(demand.dest2());
				writer.write(String.format("%s %s %s %s\n", s1, s2, t1, t2));
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
