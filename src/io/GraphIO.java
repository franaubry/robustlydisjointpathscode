package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

import dataStructures.Index;
import graphs.Edge;
import graphs.Graph;

public class GraphIO {

	public static Graph read(String path, boolean undirected, boolean indexed) {
		try {
			if(path.endsWith(".g")) {
				return readDotG(path, undirected, indexed);
			} else if(path.endsWith(".ntfl")) {
				return readDotNTFL(path, undirected);
			} else if(path.endsWith(".graph")) {
				return readDotGraph(path);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Graph read(String path) {
		try {
			if(path.endsWith(".g")) {
				return readDotG(path, true, true);
			} else if(path.endsWith(".ntfl")) {
				return readDotNTFL(path, true);
			} else if(path.endsWith(".graph")) {
				return readDotGraph(path);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static Graph readDotGraph(String path) throws FileNotFoundException {
		Scanner reader = new Scanner(new FileReader(path));
		String[] data = reader.nextLine().split(" ");
		int V = Integer.parseInt(data[1]);
		reader.nextLine();
		Index<String> nodeLabels = new Index<>();
		for(int i = 0; i < V; i++) {
			data = reader.nextLine().split(" ");
			String label = data[0];
			double x = Double.parseDouble(data[1]);
			double y = Double.parseDouble(data[2]);
			nodeLabels.add(label);
		}
		reader.nextLine();
		data = reader.nextLine().split(" ");
		int E = Integer.parseInt(data[1]);
		reader.nextLine();
		Graph g = new Graph(nodeLabels);
		for(int i = 0; i < E; i++) {
			data = reader.nextLine().split(" ");
			int orig = Integer.parseInt(data[1]);
			int dest = Integer.parseInt(data[2]);
			int igp = Integer.parseInt(data[3]);
			int cap = Integer.parseInt(data[4]);
			int lat = Integer.parseInt(data[5]);
			g.addEdge(orig,  dest, igp, cap, lat);
		}
		String[] tmp = path.split("[/]");
		tmp = tmp[tmp.length - 1].split("[.]");
		g.setName(tmp[0]);
		reader.close();
		return g;
	}

	private static Graph readDotNTFL(String path, boolean undirected) throws FileNotFoundException {
		Scanner reader = new Scanner(new FileReader(path));
		LinkedList<Edge> edges = new LinkedList<>();
		Index<String> nodeLabels = new Index<>();
		while(reader.hasNextLine()) {
			String[] data = reader.nextLine().split(" ");
			String orig = data[0];
			String dest = data[1];
			int igp = (int)Double.parseDouble(data[2]);
			double lat = Double.parseDouble(data[3]);
			nodeLabels.add(orig);
			nodeLabels.add(dest);
			Edge edge = new Edge(nodeLabels.get(orig), nodeLabels.get(dest), igp, -1, lat);
			edges.add(edge);
		}
		reader.close();
		Graph g = new Graph(nodeLabels.size());
		for(Edge e : edges) {
			//g.addEdgeUnd(e.orig(), e.dest(), e.igp(), e.cap(), e.lat());
			g.addEdge(e.orig(),  e.dest(), e.igp(), e.cap(), e.lat());
		}
		g.setNodeLabels(nodeLabels);
		String[] tmp = path.split("[/]");
		tmp = tmp[tmp.length - 1].split("[.]");
		g.setName(tmp[0]);
		return g;
	}

	private static Graph readDotG(String path, boolean undirected, boolean indexed) throws FileNotFoundException {
		Scanner reader = new Scanner(new FileReader(path));
		LinkedList<Edge> edges = new LinkedList<>();
		Index<String> nodeLabels = new Index<>();
		HashSet<Integer> indexes = new HashSet<>();
		while(reader.hasNextLine()) {
			String[] data = reader.nextLine().split(" ");
			String orig = data[0];
			String dest = data[1];
			int igp = Integer.parseInt(data[2]);
			int cap = Integer.parseInt(data[3]);
			double lat = Double.parseDouble(data[4]);
			nodeLabels.add(orig);
			nodeLabels.add(dest);
			Edge edge = null;
			if(indexed) {
				int index = Integer.parseInt(data[5]);
				edge = new Edge(nodeLabels.get(orig), nodeLabels.get(dest), index, igp, cap, lat);
				indexes.add(index);
			} else {
				edge = new Edge(nodeLabels.get(orig), nodeLabels.get(dest), igp, cap, lat);
			}
			edges.add(edge);
		}
		reader.close();
		Graph g = new Graph(nodeLabels.size());
		int index = 0;
		for(Edge e : edges) {
			g.addEdge(e, indexed);
			if(undirected) {
				while(indexes.contains(index)) {
					index++;
				}
				Edge er = new Edge(e.dest(), e.orig(), index, e.igp(), e.cap(), e.lat());
				e.setReverse(er);
				g.addEdge(er, true);
				indexes.add(index);
			}
		}
		g.setNodeLabels(nodeLabels);
		String[] tmp = path.split("[/]");
		tmp = tmp[tmp.length - 1].split("[.]");
		g.setName(tmp[0]);
		return g;
	}

}
