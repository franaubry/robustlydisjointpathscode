package io;

import dataStructures.RDPDemand;
import dataStructures.Result;
import graphs.Graph;
import graphs.Path;
import graphs.Suurballe;
import rdp.RDPSolution;
import sr.Segmenter;
import util.SimpleJson;
import verification.RDPVerifier;

import java.util.ArrayList;

public class ResultIO {
	
	public static void writeResults(ArrayList<Result> results, long pretime, String filename) {
		SimpleJson json = new SimpleJson(filename);
		json.openDict();
		json.write("nbDemands", results.size(), true);
		json.write("pretime", pretime, true);
		json.openArray("demands");
		Graph g = results.get(0).g;
		Segmenter segmenter = new Segmenter(g);
		int foundCount = 0;
		for(int i = 0; i < results.size(); i++) {
			Result res = results.get(i);
			RDPDemand demand = res.demand;
			json.openDict();
			json.write("s1", g.getLabel(demand.source1()), true);
			json.write("s2", g.getLabel(demand.source2()), true);
			json.write("t1", g.getLabel(demand.dest1()), true);
			json.write("t2", g.getLabel(demand.dest2()), true);
			// run Suurballe
			Suurballe suurballe = res.suurballe;
			Path[] paths = suurballe.getPaths();
			json.write("suurballeNbPaths", paths.length, true);
			json.write("suurballeTime", res.durationSuurballe, true);
			if(paths.length > 0) {
				json.openDict("suurballePaths");
				for(int j = 0; j < paths.length; j++) {
					json.openArray("path" + j);
					for(int k = 0; k < paths[j].size(); k++) {
						json.writeValue(g.getLabel(paths[j].get(k)), k < paths[j].size() - 1);
					}
					json.closeArray(true);
					json.write("lat" + j, paths[j].cost(), true);
					json.write("nbSeg" + j, segmenter.computSegWeight(paths[j]), j < paths.length - 1);
				}
				json.closeDict(true);
			}
			// run RDP
			RDPSolution sol = res.sol;
			if(sol.rdpExist()) {
				json.write("pathsExist", true, true);
				Path p1 = sol.getP1();
				Path p2 = sol.getP2();

				json.openArray("path1");
				for(int j = 0; j < p1.size(); j++) {
					String node = g.getLabel(p1.get(j));
					json.writeValue(node, j < p1.size() - 1);
				}
				json.closeArray(true);
				json.write("lat1", p1.getLat(), true);

				json.openArray("path2");
				for(int j = 0; j < p2.size(); j++) {
					String node = g.getLabel(p2.get(j));
					json.writeValue(node, j < p2.size() - 1);
				}
				json.closeArray(true);
				json.write("lat2", p2.getLat(), true);	
				json.write("maxlat", Math.max(p1.getLat(), p2.getLat()), true);	
				foundCount += 1;
			} else {
				json.write("pathsExist", false, true);
			}
			json.write("runtime", res.durationRDP, true);
			double nomLat1 = res.nomlat1;
			double nomLat2 = res.nomlat2;
			json.write("nomLat1", nomLat1, true);
			json.write("nomLat2", nomLat2, true);
			json.write("maxNomLat", Math.max(nomLat1, nomLat2), true);
			json.write("mincut", res.mincut, false);
			
			json.closeDict(i < results.size() - 1);
		}
		json.closeArray(true);
		json.write("demandsWithSol", foundCount, false);
		json.closeDict(false);
		json.close();

	}

}
