package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Scanner;

import graphs.Edge;
import graphs.EdgeIndexMapper;
import graphs.Graph;

public class FailureIO {

	public static BitSet[] readFailures(Graph g, String failurefn) {
		EdgeIndexMapper eim = new EdgeIndexMapper(g);
		ArrayList<BitSet> failures = new ArrayList<>();
		try {
			Scanner reader = new Scanner(new FileReader(failurefn));
			while(reader.hasNext()) {
				int fsize = reader.nextInt();
				BitSet f = new BitSet();
				for(int i = 0; i < fsize; i++) {
					String orig = reader.next();
					String dest = reader.next();
					Edge e = eim.getEdgeBetween(orig, dest);
					f.set(e.index());
				}
				failures.add(f);
			}
			reader.close();
			BitSet[] ret = new BitSet[failures.size()];
			for(int i = 0; i < failures.size(); i++) {
				ret[i] = failures.get(i);
			}
			return ret;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}