package generation;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;

import dataStructures.RDPDemand;
import graphs.Graph;

public class DemandGenerator {
	
	public static ArrayList<RDPDemand> genRandomDemands(Graph g, int nbDemands) {
		Random rnd = new Random();
		HashSet<RDPDemand> demands = new HashSet<>();
		BitSet set = new BitSet();
		while(demands.size() < nbDemands) {
			int s1 = rnd.nextInt(g.V());
			int s2 = rnd.nextInt(g.V());
			int d1 = rnd.nextInt(g.V());
			int d2 = rnd.nextInt(g.V());
			set.set(s1);
			set.set(s2);
			set.set(d1);
			set.set(d2);
			if(set.cardinality() == 4) {
				demands.add(new RDPDemand(s1, s2, d1, d2));
			}
			set.clear();
		}
		ArrayList<RDPDemand> demandL = new ArrayList<>();
		demandL.addAll(demands);
		return demandL;
	}

}
