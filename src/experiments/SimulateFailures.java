package experiments;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import graphs.Graph;
import graphs.Path;
import io.GraphIO;
import util.SimpleJson;
import verification.Tools;

public class SimulateFailures {

	static int BOTH_DISCONNECTED = 0;
	static int ONE_DISCONNECTED = 1;
	static int NOT_DISJOINT = 2;
	static int DISJOINT = 3;

	public static void main(String[] args) throws FileNotFoundException {
		String[] topo = new String[] {
				//"rf/1221.ntfl",
				//"rf/1239.ntfl",
				//"rf/1755.ntfl",
				//"rf/3257.ntfl",
				//"rf/3967.ntfl",
				//"rf/6461.ntfl",
				//"real/real1.ntfl",
				//"real/real2.ntfl",
				//"real/real3.ntfl",
				"zoo_large/Cogentco.graph",
				"zoo_large/Colt.graph",
				"zoo_large/Deltacom.graph",
				"zoo_large/DialtelecomCz.graph",
				"zoo_large/GtsCe.graph",
				"zoo_large/Interoute.graph",
				"zoo_large/Ion.graph",
				"zoo_large/TataNld.graph",
				"zoo_large/UsCarrier.graph"	
		};

		for(String topology : topo) {
			topology = "topologies/" + topology;
			Graph g = GraphIO.read(topology);
			String pathfn = "sim/" + g.getName() + ".tmp";
			int minFailSize = Integer.parseInt(args[1]);
			int maxFailSize = Integer.parseInt(args[2]);
			int nbruns = Integer.parseInt(args[3]);
			Scanner reader = new Scanner(new FileReader(pathfn));
			ArrayList<Path[]> paths = new ArrayList<>();
			while(reader.hasNextLine()) {
				String[] path1 = reader.nextLine().split(" ");
				String[] path2 = reader.nextLine().split(" ");
				Path p1 = new Path();
				for(String s : path1) p1.add(g.getIndex(s));
				Path p2 = new Path();
				for(String s : path2) p2.add(g.getIndex(s));
				paths.add(new Path[] {p1, p2});
			}
			reader.close();
			SimpleJson json = new SimpleJson("results/simulation/" + g.getName() + ".json");
			json.openDict();
			json.openDict("random");
			for(int size = minFailSize; size <= maxFailSize; size++) {
				json.openDict(size + "");
				int[] res = simulateRandomFailures(g, paths, size, nbruns);
				int sum = 0;
				for(int i = 0; i < res.length; i++) {
					sum += res[i];
				}
				json.write("both_disconnected", 100.0 * res[BOTH_DISCONNECTED] / sum, true);
				json.write("one_disconnected", 100.0 * res[ONE_DISCONNECTED] / sum, true);
				json.write("not_disjoint", 100.0 * res[NOT_DISJOINT] / sum, true);
				json.write("disjoint", 100.0 * res[DISJOINT] / sum, false);
				json.closeDict(size < maxFailSize);
			}
			json.closeDict(true);
			json.openDict("worst_init");
			for(int size = minFailSize; size <= maxFailSize; size++) {
				json.openDict(size + "");
				int[] res = simulateWorstInitFailures(g, paths, size, nbruns);	
				int sum = 0;
				for(int i = 0; i < res.length; i++) {
					sum += res[i];
				}

				json.write("both_disconnected", 100.0 * res[BOTH_DISCONNECTED] / sum, true);
				json.write("one_disconnected", 100.0 * res[ONE_DISCONNECTED] / sum, true);
				json.write("not_disjoint", 100.0 * res[NOT_DISJOINT] / sum, true);
				json.write("disjoint", 100.0 * res[DISJOINT] / sum, false);
				json.closeDict(size < maxFailSize);
			}
			json.closeDict(true);
			json.openDict("worst");
			for(int size = minFailSize; size <= maxFailSize; size++) {
				json.openDict(size + "");
				int[] res = simulateWorstFailures(g, paths, size, nbruns);	
				int sum = 0;
				for(int i = 0; i < res.length; i++) {
					sum += res[i];
				}

				json.write("both_disconnected", 100.0 * res[BOTH_DISCONNECTED] / sum, true);
				json.write("one_disconnected", 100.0 * res[ONE_DISCONNECTED] / sum, true);
				json.write("not_disjoint", 100.0 * res[NOT_DISJOINT] / sum, true);
				json.write("disjoint", 100.0 * res[DISJOINT] / sum, false);
				json.closeDict(size < maxFailSize);
			}
			json.closeDict(false);
			json.closeDict(false);
			json.close();
			System.out.println("done");
		}

	}

	static int[] simulateRandomFailures(Graph g, ArrayList<Path[]> paths, int size, int nbruns) {
		int[] res = new int[4];
		Random rnd = new Random();
		int p = -1;
		for(Path[] pp : paths) {
			p+=1;
			Path p1 = pp[0];
			Path p2 = pp[1];
			for(int i = 0; i < nbruns; i++) {
				System.out.println(size + " " + p + " " + " " + i);

				// generate a random failure set with size elements
				HashSet<Integer> fail = new HashSet<>();
				while(fail.size() != 2 * size) { // 2 * because of reverse edges
					int idx = rnd.nextInt(g.E());
					int idxRev = g.getEdge(idx).getReverse().index();
					fail.add(idx);
					fail.add(idxRev);
				}
				// deactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, false);
				}
				// check status
				int nbp = 0;
				if(Tools.srPathExists(g, p1)) nbp += 1;
				if(Tools.srPathExists(g, p2)) nbp += 1;
				if(nbp == 2) {
					// both paths exist, check disjoint
					if(Tools.areDisjoint(g, p1, p2)) {
						res[DISJOINT] += 1;
					} else {
						res[NOT_DISJOINT] += 1;
					}
				} else if(nbp == 1) {
					res[ONE_DISCONNECTED] += 1;
				}  else  {
					res[BOTH_DISCONNECTED] += 1;
				}
				// reactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, true);
				}
			}
		}
		return res;
	}

	static int[] bitsetToArray(BitSet s) {
		int k = 0;
		int[] a = new int[s.cardinality()];
		for(int i = 0; i < a.length; i++) {
			a[i] = s.nextSetBit(k);
			k = a[i] + 1;
		}
		return a;
	}

	static int[] simulateWorstInitFailures(Graph g, ArrayList<Path[]> paths, int size, int nbruns) {
		int[] res = new int[4];
		int p = -1;
		for(Path[] pp : paths) {
			p += 1;
			Path p1 = pp[0];
			Path p2 = pp[1];
			BitSet forw1 = Tools.computeForw(g, p1);
			BitSet forw2 = Tools.computeForw(g, p2);
			BitSet tmp = new BitSet();
			tmp.or(forw1);
			tmp.or(forw2);
			int[] target = bitsetToArray(tmp);
			shuffleArray(target);
			for(int i = 0; i < nbruns; i++) {
				System.out.println(size + " " + p + " " + " " + i);

				// generate a random failure set with size elements
				HashSet<Integer> fail = new HashSet<>();
				int k = 0;
				while(fail.size() != 2 * size && k < target.length) {
					int idx = target[k];
					int idxRev = g.getEdge(idx).getReverse().index();
					fail.add(idx);
					fail.add(idxRev);
					k += 1;
				}
				Random rnd = new Random();
				while(fail.size() != 2 * size) { // 2 * because of reverse edges
					int idx = rnd.nextInt(g.E());
					int idxRev = g.getEdge(idx).getReverse().index();
					fail.add(idx);
					fail.add(idxRev);
				}
				// deactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, false);
				}
				// check status
				int nbp = 0;
				if(Tools.srPathExists(g, p1)) nbp += 1;
				if(Tools.srPathExists(g, p2)) nbp += 1;
				if(nbp == 2) {
					// both paths exist, check disjoint
					if(Tools.areDisjoint(g, p1, p2)) {
						res[DISJOINT] += 1;
					} else {
						res[NOT_DISJOINT] += 1;
					}
				} else if(nbp == 1) {
					res[ONE_DISCONNECTED] += 1;
				}  else  {
					res[BOTH_DISCONNECTED] += 1;
				}
				// reactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, true);
				}
			}
		}
		return res;
	}


	static int[] simulateWorstFailures(Graph g, ArrayList<Path[]> paths, int size, int nbruns) {
		int[] res = new int[4];
		int p = -1;
		for(Path[] pp : paths) {
			p += 1;
			Path p1 = pp[0];
			Path p2 = pp[1];
			for(int i = 0; i < nbruns; i++) {
				System.out.println(size + " " + p + " " + " " + i);

				// generate a random failure set with size elements
				HashSet<Integer> fail = generateWorstFailureSet(g, p1, p2, size);
				// deactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, false);
				}
				// check status
				int nbp = 0;
				if(Tools.srPathExists(g, p1)) nbp += 1;
				if(Tools.srPathExists(g, p2)) nbp += 1;
				if(nbp == 2) {
					// both paths exist, check disjoint
					if(Tools.areDisjoint(g, p1, p2)) {
						res[DISJOINT] += 1;
					} else {
						res[NOT_DISJOINT] += 1;
					}
				} else if(nbp == 1) {
					res[ONE_DISCONNECTED] += 1;
				}  else  {
					res[BOTH_DISCONNECTED] += 1;
				}
				// reactivate all edges in the failure set and check for disjointness
				for(int idx : fail) {
					g.setActive(idx, true);
				}
			}
		}
		return res;
	}

	static HashSet<Integer> generateWorstFailureSet(Graph g, Path p1, Path p2, int size) {
		Random rnd = new Random();
		HashSet<Integer> fail = new HashSet<>();
		for(int i = 0; i < size; i++) {
			BitSet forw1 = Tools.computeForw(g, p1);
			BitSet forw2 = Tools.computeForw(g, p2);
			BitSet tmp = new BitSet();
			tmp.or(forw1);
			tmp.or(forw2);
			int[] target = bitsetToArray(tmp);
			shuffleArray(target);
			boolean found = false;
			for(int j = 0; j < target.length; j++) {
				int idx = target[j];
				if(!fail.contains(idx)) {
					// we found a link to kill
					fail.add(idx);
					int idxRev = g.getEdge(idx).getReverse().index();
					fail.add(idxRev);
					g.setActive(idx, false);
					g.setActive(idxRev, false);
					found = true;
					break;
				}
			}
			if(!found) {
				break;
			}
		}
		for(int idx : fail) g.setActive(idx, true);
		while(fail.size() != 2 * size) { // 2 * because of reverse edges
			int idx = rnd.nextInt(g.E());
			int idxRev = g.getEdge(idx).getReverse().index();
			fail.add(idx);
			fail.add(idxRev);
		}
		return fail;
	}

	// Implementing Fisher–Yates shuffle
	static void shuffleArray(int[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}


}


