package experiments;

import java.util.BitSet;

import graphs.Graph;
import io.GraphIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;

public class PrecomputeTime {

	public static void main(String[] args) {
		String[] topo = new String[] {
				"real/real1.ntfl",
				"real/real2.ntfl",
				"real/real3.ntfl",
				"rf/1221.ntfl",
				"rf/1239.ntfl",
				"rf/1755.ntfl",
				"rf/3257.ntfl",
				"rf/3967.ntfl",
				"rf/6461.ntfl",
				"zoo_large/Cogentco.graph",
				"zoo_large/Colt.graph",
				"zoo_large/Deltacom.graph",
				"zoo_large/DialtelecomCz.graph",
				"zoo_large/GtsCe.graph",
				"zoo_large/Interoute.graph",
				"zoo_large/Ion.graph",
				"zoo_large/TataNld.graph",
				"zoo_large/UsCarrier.graph"	
		};
		
		for(String tp : topo) {
			Graph g = GraphIO.read("topologies/" + tp);
			System.out.println(g.V() + " " + g.E());
			BitSet[] F = FailureGen.genAllEdges(g);
			long start = System.nanoTime();
			new ForwFailOneSidedFailures(g, F);
			long end = System.nanoTime();
			System.out.println(tp + ":  " + (end - start) / 1e9);
		}

	}
	
}
