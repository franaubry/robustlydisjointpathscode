package experiments;

import java.io.File;
import java.util.ArrayList;

import dataStructures.RDPDemand;
import graphs.Graph;
import graphs.Path;
import graphs.Suurballe;
import io.DemandIO;
import io.GraphIO;
import rdp.Forw;
import sr.Segmenter;
import util.SimpleJson;

public class RunSuurballe {

	public static void main(String[] args) {
		String[] dirs = new String[] {"rf", "real", "zoo_large"};
		for(String dir : dirs) {
			File d = new File("topologies/" + dir);
			for(String fn : d.list()) {
				System.out.println(fn);
				String name = fn.split("[.]")[0];
				Graph g = GraphIO.read("topologies/" + dir + "/" + fn);
				Segmenter seg = new Segmenter(g);
				Forw forw = new Forw(g);
				ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, "demands/" + dir + "/" + name + ".dem");
				SimpleJson json = new SimpleJson("./results/suurballe/" + name + ".json");
				json.openDict();
				for(int j = 0; j < demands.size(); j++) {
					json.openDict("demand" + j);
					RDPDemand demand = demands.get(j);
					json.write("s1", g.getLabel(demand.source1()), true);
					json.write("s2", g.getLabel(demand.source2()), true);
					json.write("t1", g.getLabel(demand.dest1()), true);
					json.write("t2", g.getLabel(demand.dest2()), true);
					long startTime = System.nanoTime();
					Suurballe suurballe = new Suurballe(g, demand);
					long endTime = System.nanoTime();
					json.write("time", endTime - startTime, true);
					Path[] paths = suurballe.getPaths();
					json.write("nbpaths", paths.length, true);
					json.write("nomlat1", forw.forwLat(demand.source1(), demand.dest1()), true);
					json.write("nomlat2", forw.forwLat(demand.source2(), demand.dest2()), true);
					json.openArray("paths");
					for(int i = 0; i < paths.length; i++) {
						json.openDict();
						int nbseg = seg.computSegWeight(paths[i]);
						json.write("nbseg", nbseg, true);
						json.write("lat", paths[i].cost(), false);
						json.closeDict(i < paths.length - 1);
					}
					json.closeArray(false);
					json.closeDict(j < demands.size() - 1);
				}
				json.closeDict(false);
				json.close();
			}
		}
		System.out.println("done");
	}
	
}
