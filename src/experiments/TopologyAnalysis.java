package experiments;

import java.io.File;

import graphs.BFS;
import graphs.Edge;
import graphs.Graph;
import graphs.MaxEdgeDisjointPaths;
import graphs.Path;
import io.GraphIO;
import util.SimpleJson;

public class TopologyAnalysis {
	
	public static void main(String[] args) {
		String[] dirs = new String[] {"zoo_large", "real", "rf"};
		for(String d : dirs) {
			File f = new File("topologies/" + d + "/");
			for(String instancefn : f.list()) {
				System.out.println(instancefn);
				Graph g = GraphIO.read("topologies/" + d + "/" + instancefn);
				analyze(g);
			}
		}
		System.out.println("done");
	}
	
	static void analyze(Graph g) {
		// analyze min cuts
		int[] count = new int[g.E()];
		int max = 0;
		for(int u = 0; u < g.V(); u++) {
			for(int v = 0; v < g.V(); v++) {
				if(u == v) continue;
				MaxEdgeDisjointPaths medp = new MaxEdgeDisjointPaths(g, new int[] {u}, new int[] {v});
				int mincut = medp.getMaxFlow();
				if(g.getName().equals("1239") && mincut == 60) {
					int[][] flow = new int[g.V()][g.V()];
					Path[] paths = medp.getPaths();
					for(Path p : paths) {
						for(int i = 1; i < p.size(); i++) {
							flow[p.get(i - 1)][p.get(i)] += 1;
						}
					}
				}
				count[mincut] += 1;
				max = Math.max(max, mincut);
			}
		}
		int nbcc = BFS.nbConnectedComponents(g);
		SimpleJson json = new SimpleJson("results/topoAnalysis/" + g.getName() + ".json");
		json.openDict();
		json.write("V", g.V(), true);
		json.write("E", g.E(), true);
		json.write("nbConnectedComponents", nbcc, true);
		int[][] linkcnt = new int[g.V()][g.V()];
		for(int i = 0; i < g.V(); i++) {
			for(Edge e : g.outEdges(i)) {
				linkcnt[i][e.dest()] += 1;
			}
		}
		boolean undirected = true;
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				if(linkcnt[i][j] != linkcnt[j][i]) undirected = false;
			}
		}
		json.write("undirected", undirected, true);
		json.openDict("parallel");
			
		int maxparallel = 0;
		int[] parallelcnt = new int[g.E()];
		for(int i = 0; i < g.V(); i++) {
			for(int j = 0; j < g.V(); j++) {
				parallelcnt[linkcnt[i][j]] += 1;
				maxparallel = Math.max(maxparallel, parallelcnt[linkcnt[i][j]]);
			}
		}
		for(int i = 0; i < parallelcnt.length; i++) {
			if(i == 0 || parallelcnt[i] > 0) {
				json.write(i + "", parallelcnt[i], i < maxparallel);
			}
		}
		json.closeDict(true);
		json.openDict("mincuts");
		for(int i = 0; i < count.length; i++) {
			if(count[i] > 0) {
				json.write(i + "", count[i], i < max);
			}
		}
		json.closeDict(false);
		json.closeDict(false);
		json.close();
	}
	
}
