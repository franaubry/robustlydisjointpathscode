package experiments;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.BitSet;

import dataStructures.RDPDemand;
import graphs.Graph;
import graphs.Path;
import graphs.Suurballe;
import io.DemandIO;
import io.GraphIO;
import rdp.FailureGen;
import rdp.ForwFailOneSidedFailures;
import rdp.RDPOneSidedFailures;
import rdp.RDPSolution;
import sr.Segmenter;
import util.SimpleJson;
import util.Timer;
import verification.RDPVerifier;

public class RunDemandsOneSidedFailures {

	public static void main(String[] args) throws FileNotFoundException {
		String instancefn = args[0];
		String demandfn = args[1];
		int maxSeg = Integer.parseInt(args[2]);
		Graph g = GraphIO.read(instancefn);
		System.out.println("instance: " + instancefn);
		System.out.println("max-seg: " + maxSeg);
		System.out.println("V: " + g.V());
		System.out.println("E: " + g.E());
		ArrayList<RDPDemand> demands = DemandIO.readRDPDemands(g, demandfn);
		BitSet[] F = FailureGen.genAllEdges(g);
		Segmenter segmenter = new Segmenter(g);
		Timer timer = new Timer();
		// initialize RDP
		System.out.println("initializing RDP");
		timer.createAndStart("init_rdp");
		RDPOneSidedFailures rdp = new RDPOneSidedFailures(g, F);
		ForwFailOneSidedFailures ffs = rdp.getFFS();
		timer.stop("init_rdp");
		System.out.println("finished. time = " + timer.getSeconds("init_rdp") + "s");
		// compute the paths
		int foundCount = 0;
		SimpleJson json = new SimpleJson("results/runDemands/" + g.getName() + "_" + maxSeg + ".json");
		json.openDict();
		json.write("maxSeg", maxSeg, true);
		json.write("nbDemands", demands.size(), true);
		json.openArray("demands");
		for(int i = 0; i < demands.size(); i++) {
			RDPDemand demand = demands.get(i);
			json.openDict();
			json.write("s1", g.getLabel(demand.source1()), true);
			json.write("s2", g.getLabel(demand.source2()), true);
			json.write("t1", g.getLabel(demand.dest1()), true);
			json.write("t2", g.getLabel(demand.dest2()), true);
			// run Suurballe
			timer.createAndStart("suurballe_" + i);
			Suurballe suurballe = new Suurballe(g, demand);
			Path[] paths = suurballe.getPaths();
			timer.stop("suurballe_" + i);
			json.write("suurballeNbPaths", paths.length, true);
			json.write("suurballeTime", timer.getNano("suurballe_" + i), true);

			if(paths.length > 0) {
				json.openDict("suurballePaths");
				for(int j = 0; j < paths.length; j++) {
					json.openArray("path" + j);
					for(int k = 0; k < paths[j].size(); k++) {
						json.writeValue(g.getLabel(paths[j].get(k)), k < paths[j].size() - 1);
					}
					json.closeArray(true);
					json.write("lat" + j, paths[j].cost(), true);
					json.write("nbSeg" + j, segmenter.computSegWeight(paths[j]), j < paths.length - 1);
				}
				json.closeDict(true);
			}
			// run RDP
			int percent = (int)((100.0 * i) / demands.size());
			System.out.printf("progress: %d%% | current demand: {%s}\n", percent, demand.toString(g));
			timer.createAndStart("run_demand_" + i);
			RDPSolution sol = rdp.findPaths(demands.get(i), maxSeg);
			timer.stop("run_demand_" + i);
			if(sol.rdpExist()) {
				json.write("pathsExist", true, true);
				Path p1 = sol.getP1();
				Path p2 = sol.getP2();

				json.openArray("path1");
				for(int j = 0; j < p1.size(); j++) {
					String node = g.getLabel(p1.get(j));
					json.writeValue(node, j < p1.size() - 1);
				}
				json.closeArray(true);
				json.write("lat1", p1.getLat(), true);

				json.openArray("path2");
				for(int j = 0; j < p2.size(); j++) {
					String node = g.getLabel(p2.get(j));
					json.writeValue(node, j < p2.size() - 1);
				}
				json.closeArray(true);
				json.write("lat2", p2.getLat(), true);	

				json.write("maxlat", Math.max(p1.getLat(), p2.getLat()), true);	

				RDPVerifier verifier = new RDPVerifier(g, rdp.getFFS(), p1, p2, F);
				if(!verifier.check()) {
					json.write("verification", false, false);
					verifier.printReport();;
					System.exit(0);
				}
				foundCount += 1;
			} else {
				json.write("pathsExist", false, true);
			}
			json.write("runtime", timer.getNano("run_demand_" + i), true);

			double nomLat1 = ffs.forwLat(demand.source1(), demand.dest1());
			double nomLat2 = ffs.forwLat(demand.source2(), demand.dest2());

			json.write("nomLat1", nomLat1, true);
			json.write("nomLat2", nomLat2, true);
			json.write("maxNomLat", Math.max(nomLat1, nomLat2), false);

			json.closeDict(i < demands.size() - 1);
		}
		double totalDemandTime = 0;
		for(int i = 0; i < demands.size(); i++) {
			totalDemandTime += timer.getSeconds("run_demand_" + i);
		}
		json.closeArray(true);
		json.write("averageTime", totalDemandTime / demands.size(), true);
		json.write("demandsWithSol", foundCount, false);
		System.out.printf("done! %% with sol: %d | average time: %d\n", (int)((100.0 * foundCount) / demands.size()), (int)(totalDemandTime / demands.size()));
		json.closeDict(false);
		json.close();
	}

}
