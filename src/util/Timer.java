package util;

import java.util.HashMap;
public class Timer {

	private HashMap<String, Long> timers;
	private HashMap<String, Long> timersStart;
	
	public Timer() {
		timers = new HashMap<>();
		timersStart = new HashMap<>();
	}
	
	public void createTimer(String lbl) {
		timers.put(lbl, 0l);
	}
	
	public void createAndStart(String lbl) {
		timers.put(lbl, 0l);
		start(lbl);
	}
	
	public void start(String lbl) {
		timersStart.put(lbl, System.nanoTime());
	}
	
	public void stop(String lbl) {
		long time = System.nanoTime() - timersStart.get(lbl);
		timers.put(lbl, timers.get(lbl) + time);
	}
	
	public int getSeconds(String lbl) {
		return (int)(timers.get(lbl) / 1e9);
	}
	
	public long getNano(String lbl) {
		return timers.get(lbl);
	}
	
}
