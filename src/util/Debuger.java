package util;

import java.util.BitSet;
import java.util.LinkedList;

import graphs.Edge;
import graphs.EdgeIndexMapper;
import graphs.Graph;
import rdp.ForwFailGeneral;

public class Debuger {
	
	private Graph g;
	private ForwFailGeneral ffs;
	private BitSet[] F;
	private EdgeIndexMapper eim;
	
	public Debuger(Graph g, BitSet[] F) {
		this.g = g;
		this.F = F;
		eim = new EdgeIndexMapper(g);
		ffs = new ForwFailGeneral(g, F);
	}
	
	public void printForwEdges(int x, int y) {
		System.out.println(String.format("edges in forw(%d,%d)=forw(%s,%s)", x, y, g.getLabel(x), g.getLabel(y)));
		printEdges(ffs.forw(x, y));
	}
	
	public void printEdges(BitSet s) {
		LinkedList<Edge> edges = eim.getEdges(s);
		for(Edge e : edges) {
			System.out.println(e.toString());
		}	
	}
	
	public void printForwIntersection(int x1, int y1, int x2, int y2) {
		BitSet inter = new BitSet();
		inter.or(ffs.forw(x1, y1));
		inter.and(ffs.forw(x2, y2));
		printEdges(inter);
	}
	

}
