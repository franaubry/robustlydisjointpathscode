package util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SimpleJson {
	
	private PrintWriter writer;
	private int openCount;
	
	public SimpleJson(String filename) {
		try {
			writer = new PrintWriter(new FileWriter(filename));
			openCount = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String makeTabs() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < openCount; i++) {
			sb.append("\t");
		}
		return sb.toString();
	}
	
	public void openArray() {
		writer.println(makeTabs() + "[");
		openCount += 1;
	}
	
	public void openArray(String key) {
		writer.println(String.format("%s\"%s\": [", makeTabs(), key));
		openCount += 1;
	}

	public void closeArray(boolean hasNext) {
		openCount -= 1;
		writer.println(makeTabs() + "]"  + (hasNext ? "," : ""));
	}
	
	public void openDict() {
		writer.println(makeTabs() + "{");
		openCount += 1;
	}
	
	public void openDict(String key) {
		writer.println(String.format("%s\"%s\": {", makeTabs(), key));
		openCount += 1;
	}
	
	public void closeDict(boolean hasNext) {
		openCount -= 1;
		writer.println(makeTabs() + "}" + (hasNext ? "," : ""));
	}
	
	public void write(String key, String value, boolean hasNext) {
		writer.println(String.format("%s\"%s\": \"%s\"%s", makeTabs(), key, value, (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void writeValue(String value, boolean hasNext) {
		writer.println(String.format("%s\"%s\"%s", makeTabs(), value, (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void write(String key, int value, boolean hasNext) {
		writer.println(String.format("%s\"%s\": %d%s", makeTabs(), key, value, (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void write(String key, long value, boolean hasNext) {
		writer.println(String.format("%s\"%s\": %d%s", makeTabs(), key, value, (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void write(String key, double value, boolean hasNext) {
		writer.println(String.format("%s\"%s\": %f%s", makeTabs(), key, value, (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void write(String key, boolean value, boolean hasNext) {
		writer.println(String.format("%s\"%s\": %s%s", makeTabs(), key, Boolean.toString(value), (hasNext ? "," : "")));
		writer.flush();
	}
	
	public void close() {
		writer.close();
	}

}
