package graphs;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.TreeSet;

public class Dijkstra {

	public static Graph forwDAG(Graph g, int orig) {
		return shortestPathDAG(g, orig, g.getIgpWeights());
	}
	
	public static Graph subDag(Graph dag, int t) {
		Graph sub = new Graph(dag.V());
		Queue<Integer> Q = new LinkedList<>();
		Q.add(t);
		BitSet visited = new BitSet();
		visited.set(t);
		while(!Q.isEmpty()) {
			int cur = Q.poll();
			for(Edge e : dag.inEdges(cur)) {
				int y = e.orig();
				sub.addEdge(e, true);
				if(!visited.get(y)) {
					visited.set(y);
					Q.add(y);
				}
			}
		}
		return sub;
	}
	
	public static Edge[] shortestPathTree(Graph g, int orig, double[] weight) {
		// initialize distance array
		double[] distance = new double[g.V()];
		Arrays.fill(distance, Double.POSITIVE_INFINITY);
		distance[orig] = 0;
		// initialize parent array
		Edge[] parent = new Edge[g.V()];
		// initialize path length (edges)
		int[] length = new int[g.V()];
		Arrays.fill(length, -1);
		length[orig] = 0;
		// compute shortest paths
		TreeSet<Integer> Q = new TreeSet<>(new VertexCmp(distance));
		Q.add(orig);
		while(!Q.isEmpty()) {
			int v = Q.pollFirst();	
			for(Edge e : g.outEdges(v)) {
				int u = e.dest();
				if(!g.isActive(e)) continue;
				if(distance[v] + weight[e.index()] < distance[u]) {
					Q.remove(u);
					distance[u] = distance[v] + weight[e.index()];
					length[u] = length[v] + 1;
					parent[u] = e;
					Q.add(u);
				}
			} 
		}
		return parent;
	}
	
	public static Path buildPath(Edge[] parent, int orig, int dest) {
		if(parent[dest] == null) return null;
		Path path = new Path();
		int cur = dest;
		while(parent[cur] != null) {
			path.add(cur);
			cur = parent[cur].orig();
		}
		if(cur != orig) return null;
		path.add(orig);
		path.reverse();
		return path;
	}

	@SuppressWarnings("unchecked")
	public static Graph shortestPathDAG(Graph g, int orig, double[] weight) {
		// initialize distance array
		double[] distance = new double[g.V()];
		Arrays.fill(distance, Double.POSITIVE_INFINITY);
		distance[orig] = 0;
		// initialize parent array
		List<Edge>[] parent = new LinkedList[g.V()];
		for(int i = 0; i < g.V(); i++) {
			parent[i] = new LinkedList<>();
		}
		// initialize path length (edges)
		int[] length = new int[g.V()];
		Arrays.fill(length, -1);
		length[orig] = 0;
		// compute shortest paths
		TreeSet<Integer> Q = new TreeSet<>(new VertexCmp(distance));
		Q.add(orig);
		while(!Q.isEmpty()) {
			int v = Q.pollFirst();	
			for(Edge e : g.outEdges(v)) {
				int u = e.dest();
				if(!g.isActive(e)) continue;
				if(distance[v] + weight[e.index()] < distance[u]) {
					Q.remove(u);
					distance[u] = distance[v] + weight[e.index()];
					length[u] = length[v] + 1;
					parent[u].clear();
					parent[u].add(e);
					Q.add(u);
				} else if(distance[v] + weight[e.index()] == distance[u]) {
					parent[u].add(e);
				}
			} 
		}
		// build the shortest path DAG
		Graph dag = new Graph(g.V());
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : parent[v]) {
				dag.addEdge(e, true);
			}
		}
		dag.setNodeLabels(g.getNodeLabels());
		return dag;
	}
	
	/*
	 * Class used to compare vertices by distance.
	 */
	private static class VertexCmp implements Comparator<Integer> {

		private double[] distance;

		public VertexCmp(double[] distance) {
			this.distance = distance;
		}

		public int compare(Integer o1, Integer o2) {
			int dcmp = Double.compare(distance[o1], distance[o2]);
			if(dcmp == 0) return o1 - o2;
			return dcmp;
		}
	}


}
