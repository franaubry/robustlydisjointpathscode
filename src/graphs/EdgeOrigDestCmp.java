package graphs;

import java.util.Comparator;

/**
 * Comparator to sort edges by origin (ties broken by destination).
 */
public class EdgeOrigDestCmp implements Comparator<Edge> {

	public int compare(Edge e1, Edge e2) {
		int dorig = e1.orig() - e2.orig();
		if(dorig != 0) return dorig;
		return e1.dest() - e2.dest();
	}

}