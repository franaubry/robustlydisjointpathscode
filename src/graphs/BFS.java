package graphs;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {

	public static int nbConnectedComponents(Graph g) {
		BitSet visited = new BitSet();
		int nbcc = 0;
		for(int s = 0; s < g.V(); s++) {
			if(visited.get(s)) continue;
			nbcc += 1;
			Queue<Integer> Q = new LinkedList<>();
			Q.add(s);
			while(!Q.isEmpty()) {
				int cur = Q.poll();
				for(Edge e : g.outEdges(cur)) {
					if(!g.isActive(e)) continue;
					if(!visited.get(e.dest())) {
						Q.add(e.dest());
						visited.set(e.dest());
					}
				}
			}
		}
		return nbcc;
	}

	public static boolean pathExists(Graph g, int s, int t) {
		BitSet visited = new BitSet();
		Queue<Integer> Q = new LinkedList<>();
		Q.add(s);
		while(!Q.isEmpty()) {
			int cur = Q.poll();
			for(Edge e : g.outEdges(cur)) {
				if(!g.isActive(e)) continue;
				if(!visited.get(e.dest())) {
					Q.add(e.dest());
					visited.set(e.dest());
				}
			}
		}
		return visited.get(t);
	}

}
