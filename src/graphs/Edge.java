package graphs;

public class Edge {
	
	private int orig, dest, index, igp, cap, flow;
	private double lat, cost;
	private Edge reverse;
	
	public Edge(int orig, int dest, int index, int igp, int cap, double lat) {
		this.orig = orig;
		this.dest = dest;
		this.index = index;
		this.igp = igp;
		this.cap = cap;
		this.lat = lat;
		flow = 0;
		cost = 0;
		reverse = null;
	}
	
	public Edge(int orig, int dest, int igp, int cap, double lat) {
		this.orig = orig;
		this.dest = dest;
		this.igp = igp;
		this.cap = cap;
		this.lat = lat;
		flow = 0;
		cost = 0;
		index = -1;
		reverse = null;
	}
	
	public void setReverse(Edge edge) {
		this.reverse = edge;
		edge.reverse = this;
	}
	
	public Edge getReverse() {
		return reverse;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public int orig() {
		return orig;
	}
	
	public int dest() {
		return dest;
	}
	
	public int index() {
		return index;
	}
	
	public int igp() {
		return igp;
	}
	
	public int cap() {
		return cap;
	}
	
	public void setCap(int cap) {
		this.cap = cap;
	}
	
	public double lat() {
		return lat;
	}
	
	public int flow() {
		return flow;
	}
	
	public void setFlow(int flow) {
		this.flow = flow;
	}
	
	public double cost() {
		return cost;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public String toVerboseString() {
		return String.format("(orig=%d,dest=%d,index=%d,igp=%d,cap=%d,lat=%.3f)", orig, dest, index, igp, cap, lat);
	}
	
	public String toString(Graph g) {
		return String.format("(%s,%s)", g.getLabel(orig), g.getLabel(dest));
	}
	
	public String toString() {
		return String.format("(%d,%d)", orig, dest);
	}

}
