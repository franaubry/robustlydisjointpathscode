package graphs;

import dataStructures.SparseSet;

public class ConflictGraph {
	
	private Graph g;
	private int V;
	private SparseSet[] outN;
	
	public ConflictGraph(Graph g) {
		this.g = g;
		this.V = g.V();
		outN = new SparseSet[V];
		for(int x = 0; x < V; x++) {
			outN[x] = new SparseSet(V);
			outN[x].remove(x);
		}
	}
	
	public void createRestorePoint() {
		for(int x = 0; x < V; x++) {
			outN[x].addRestorePoint();
		}
	}
	
	public void restore() {
		for(int x = 0; x < V; x++) {
			outN[x].restore();
		}
	}
	
	public int V() {
		return V;
	}
	
	public void disconnect(int x, int y) {
		outN[x].remove(y);
		outN[y].remove(x);
	}
	
	public boolean connected(int x, int y) {
		return outN[x].contains(y);
	}
	
	public SparseSet outN(int x) {
		return outN[x];
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int v = 0; v < V(); v++) {
			sb.append(v + ":" + g.getLabel(v) + " -> ");
			for(int i = 0; i < outN[v].size(); i++) {
				int u = outN[v].get(i);
				sb.append(u + ":" + g.getLabel(u) + ", ");
			}
			sb.append('\n');
		}
		return sb.toString();
	}

}
