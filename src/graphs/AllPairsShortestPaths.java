package graphs;

import java.util.Arrays;

public class AllPairsShortestPaths {
	
	public static double[][] allPairsMinLat(Graph g) {
		return allPairsMinDist(g, g.getLatencies());
	}
	
	public static double[][] allPairsMinIGP(Graph g) {
		return allPairsMinDist(g, g.getIgpWeights());
	}
	
	public static double[][] allPairsMinDist(Graph g, double[] weight) {
		int n = g.V();
		double[][] distance = new double[n][n];
		for(int x = 0; x < n; x++) {
			Arrays.fill(distance[x], Double.POSITIVE_INFINITY);
			distance[x][x] = 0;
		}
		// base case
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				distance[e.orig()][e.dest()] = weight[e.index()];
			}
		}
		// general case
		for(int k = 0; k < n; k++) {
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					double dikj = distance[i][k] + distance[k][j];
					distance[i][j] = Math.min(distance[i][j], dikj);
				}
			}
		}
		return distance;
	}

}
