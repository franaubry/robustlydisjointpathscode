package graphs;

import java.util.ArrayList;
import java.util.BitSet;

public class Path {

	private ArrayList<Integer> nodes;
	private int size;
	private double lat, cost;
	private BitSet nodeSet;
	
	public Path() {
		nodes = new ArrayList<>();
		nodeSet = new BitSet();
		size = 0;
		lat = -1;
		cost = -1;
	}
	
	public void add(int x) {
		if(size < nodes.size()) {
			nodes.set(size, x);
		} else {
			nodes.add(x);			
		}
		nodeSet.set(x);
		size++;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public double cost() {
		return cost;
	}
	
	public int last() {
		return nodes.get(size - 1);
	}
	
	public void removeLast() {
		nodeSet.clear(last());
		size--;
	}
	
	public Path copy() {
		Path cp = new Path();
		for(int i = 0; i < size; i++) {
			cp.add(get(i));
		}
		cp.setLat(lat);
		cp.setCost(cost);
		return cp;
	}
	
	public int get(int i) {
		return nodes.get(i);
	}
	
	public int size() {
		return size;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLat() {
		return lat;
	}
	
	public void reverse() {
		for(int i = 0; i < size / 2; i++) {
			int tmp = nodes.get(i);
			nodes.set(i, nodes.get(size - i - 1));
			nodes.set(size - i - 1, tmp);
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < size; i++) {
			sb.append(nodes.get(i));
			if(i < size - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	public boolean contains(int v) {
		return nodeSet.get(v);
	}
	
	public String fileStr(Graph g) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < size; i++) {
			sb.append(g.getLabel(nodes.get(i)));
			if(i < size - 1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
	
	public String toString(Graph g) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < size; i++) {
			sb.append(g.getLabel(nodes.get(i)));
			if(i < size - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
}
