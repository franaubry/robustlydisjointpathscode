package graphs;

import dataStructures.RDPDemand;

public class Suurballe {
	
	private MinCostFlow mcf;
	
	public Suurballe(Graph g, RDPDemand demand) {
		int[] sources = {demand.source1(), demand.source2()};
		int[] destinations = {demand.dest1(), demand.dest2()};
		mcf = new MinCostFlow(g, sources, destinations);
	}
	
	public Path[] getPaths() {
		return mcf.getPaths();
	}
	
	public double getLatSum() {
		return mcf.getFlowCost();
	}

}
