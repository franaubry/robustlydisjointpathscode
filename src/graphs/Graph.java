package graphs;

import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import dataStructures.Index;

public class Graph {
	
	private int V, E;
	private String name;
	private LinkedList<Edge>[] outE, inE;
	private BitSet edgeSet, activeEdges;
	private Index<String> nodeLabels;
	private HashMap<Integer, Edge> indexToEdge;
	
	@SuppressWarnings("unchecked")
	public Graph(int V) {
		name = "unnamed";
		this.V = V;
		nodeLabels = new Index<>();
		outE = new LinkedList[V];
		inE = new LinkedList[V];
		for(int v = 0; v < V; v++) {
			nodeLabels.add("" + v);
			outE[v] = new LinkedList<>();
			inE[v] = new LinkedList<>();
		}
		this.E = 0;
		edgeSet = new BitSet();
		activeEdges = new BitSet();
		indexToEdge = new HashMap<>();
	}
	
	@SuppressWarnings("unchecked")
	public Graph(Index<String> nodeLabels) {
		name = "unnamed";
		this.nodeLabels = nodeLabels;
		V = nodeLabels.size();
		outE = new LinkedList[V];
		inE = new LinkedList[V];
		for(int v = 0; v < V; v++) {
			outE[v] = new LinkedList<>();
			inE[v] = new LinkedList<>();
		}
		this.E = 0;
		edgeSet = new BitSet();
		activeEdges = new BitSet();
		indexToEdge = new HashMap<>();
	}
	
	@SuppressWarnings("unchecked")
	public Graph(Collection<Edge> edges, boolean keepIndexes, boolean undirected) {
		name = "unnamed";
		V = 0;
		for(Edge e : edges) {
			V = Math.max(V, e.orig() + 1);
			V = Math.max(V, e.dest() + 1);
		}
		nodeLabels = new Index<>();
		outE = new LinkedList[V];
		inE = new LinkedList[V];
		for(int v = 0; v < V; v++) {
			nodeLabels.add("" + v);
			outE[v] = new LinkedList<>();
			inE[v] = new LinkedList<>();
		}
		edgeSet = new BitSet();
		activeEdges = new BitSet();
		indexToEdge = new HashMap<>();
		for(Edge e : edges) {
			addEdge(e, keepIndexes);
			indexToEdge.put(e.index(), e);
		}
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Index<String> getNodeLabels() {
		return nodeLabels;
	}
	
	public void setNodeLabels(Index<String> nodeLabels) {
		this.nodeLabels = nodeLabels;
	}
	
	public int V() {
		return V;
	}
	
	public int E() {
		return E;
	}
	
	public int getIndex(String nodeLbl) {
		return nodeLabels.get(nodeLbl);
	}
	
	public String getLabel(int v) {
		return nodeLabels.get(v);
	}
	
	public int inDeg(int v) {
		return inE[v].size();
	}
	
	public int outDeg(int v) {
		return outE[v].size();
	}
	
	public Edge[] getEdgesById() {
		Edge[] edges = new Edge[E];
		for(int v = 0; v < V(); v++) {
			for(Edge edge : outE[v]) {
				edges[edge.index()] = edge;
			}
		}
		return edges;
	}
	
	public void addEdge(int orig, int dest, int igp, int cap, double lat) {
		Edge e = new Edge(orig, dest, E, igp, cap, lat);
		addEdge(e, true);
	}
	
	public void addEdgeUnd(int orig, int dest, int igp, int cap, double lat) {
		Edge e = new Edge(orig, dest, E, igp, cap, lat);
		Edge er = new Edge(dest, orig, E + 1, igp, cap, lat);
		e.setReverse(er);
		addEdge(e, true);
		addEdge(er, true);
	}
	
	public void addEdge(Edge e, boolean keepIndex) {
		if(!keepIndex) e.setIndex(E);
		outE[e.orig()].add(e);
		inE[e.dest()].add(e);
		edgeSet.set(e.index());
		activeEdges.set(e.index());
		E++;
		indexToEdge.put(e.index(), e);
	}
	
	public double[] getLatencies() {
		double[] lat = new double[E];
		for(int v = 0; v < V; v++) {
			for(Edge e : outE[v]) {
				lat[e.index()] = e.lat();
			}
		}
		return lat;
	}
	
	public double[] getIgpWeights() {
		double[] igp = new double[E];
		for(int v = 0; v < V; v++) {
			for(Edge e : outE[v]) {
				igp[e.index()] = e.igp();
			}
		}
		return igp;
	}
	
	public boolean isActive(Edge e) {
		return activeEdges.get(e.index());
	}
	
	public BitSet activeEdges() {
		return activeEdges;
	}
	
	public LinkedList<Edge> outEdges(int v) {
		return outE[v];
	}
	
	public LinkedList<Edge> inEdges(int v) {
		return inE[v];
	}
	
	public BitSet edgeSet() {
		return edgeSet;
	}
	
	public Edge getEdge(int index) {
		return indexToEdge.get(index);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int v = 0; v < V(); v++) {
			sb.append(v + ":" + nodeLabels.get(v) + " -> ");
			for(Edge e : outE[v]) {
				sb.append(v + ":" + nodeLabels.get(e.dest()) + ", ");
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	public void setActive(int idx, boolean b) {
		if(b) activeEdges.set(idx);
		else activeEdges.clear(idx);
	}

}