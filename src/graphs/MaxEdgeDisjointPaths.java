package graphs;

import java.util.LinkedList;
import java.util.Queue;


/**
 * Implementation of the algorithm to compute the maximum
 * number of edge disjoint paths between given sources and destinations.
 * 
 * Each path can start at any source and end at any destination.
 *
 * @author f.aubry@uclouvain.be
 */
public class MaxEdgeDisjointPaths {

	private int maxflow;
	private Graph gf;
	private int S, T;
	private Path[] paths;

	public MaxEdgeDisjointPaths(Graph g, int[] sources, int[] destinations) {
		gf = new Graph(g.V() + 2);
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				Edge ef = new Edge(e.orig(), e.dest(), 0, 1, 0);
				Edge er = new Edge(e.dest(), e.orig(), 0, 0, 0);
				ef.setReverse(er);
				gf.addEdge(ef, false);
				gf.addEdge(er, false);
			}
		}
		S = g.V();
		T = g.V() + 1;
		for(int s : sources) {
			Edge ef = new Edge(S, s, 0, Integer.MAX_VALUE, 0);
			Edge er = new Edge(s, S, 0, 0, 0);
			ef.setReverse(er);
			gf.addEdge(ef, false);
			gf.addEdge(er, false);
		}
		for(int t : destinations) {
			Edge ef = new Edge(t, T, 0, Integer.MAX_VALUE, 0);
			Edge er = new Edge(T, t, 0, 0, 0);
			ef.setReverse(er);
			gf.addEdge(ef, false);
			gf.addEdge(er, false);
		}
		computeMaxFlow();
		//paths = buildPaths();
	}

	/*
	 * Auxiliary method to compute the minimum cost maximum flow.
	 */
	private void computeMaxFlow() {
		maxflow = 0;
		while(true) {
			Edge[] parent = new Edge[gf.V()];
			int[] pcap = new int[gf.V()];
			pcap[S] = Integer.MAX_VALUE;
			Queue<Integer> Q = new LinkedList<>();
			Q.add(S);
			// perform BFS iteration
			while(!Q.isEmpty()) {
				int cur = Q.poll();
				for(Edge e : gf.outEdges(cur)) {
					int next = e.dest();
					if(next != S && e.cap() > 0 && parent[next] == null) {
						parent[next] = e;
						pcap[e.dest()] = Math.min(pcap[e.orig()], e.cap());
						Q.add(next);
					}
				}
			}
			// check whether a path was found
			if(parent[T] == null) break;
			Edge cur = parent[T];
			int flow = pcap[T];
			maxflow += flow;
			while(cur != null) {
				push(cur, flow);
				cur = parent[cur.orig()];
			}
		}
	}
	
	private double push(Edge e, int flow) {
		e.setFlow(e.flow() + flow);
		e.setCap(e.cap() - flow);
		Edge er = e.getReverse();
		er.setFlow(er.flow() - flow);
		er.setCap(er.cap() + flow);
		return e.cost() * flow;
	}
	
	private Path[] buildPaths() {
		Path[] paths = new Path[maxflow];
		for(int i = 0; i < maxflow; i++) {
			// find a path with BFS
			Queue<Integer> Q = new LinkedList<>();
			Edge[] parent = new Edge[gf.V()];
			Q.add(S);
			while(!Q.isEmpty()) {
				int cur = Q.poll();
				for(Edge e : gf.outEdges(cur)) {
					if(e.dest() != S && e.flow() > 0 && parent[e.dest()] == null) {
						parent[e.dest()] = e;
						Q.add(e.dest());
					}
				}
			}
			// build the path
			Path p = new Path();
			int cur = parent[T].orig();
			double cost = 0;
			while(cur != S) {
				parent[cur].setFlow(0);
				cost += parent[cur].cost();
				p.add(parent[cur].dest());
				cur = parent[cur].orig();
			}
			p.reverse();
			p.setCost(cost);
			paths[i] = p;
		}
		return paths;
	}
	
	public Path[] getPaths() {
		return paths;
	}
	
	public int getMaxFlow() {
		return maxflow;
	}

}

