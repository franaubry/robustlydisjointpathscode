package graphs;

import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;

import dataStructures.IntPair;

public class EdgeIndexMapper {

	private Graph g;
	private HashMap<IntPair, LinkedList<Edge>> M;
	
	public EdgeIndexMapper(Graph g) {
		this.g = g;
		M = new HashMap<>();
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				IntPair key = new IntPair(v, e.dest());
				LinkedList<Edge> L = M.get(key);
				if(L == null) {
					L = new LinkedList<>();
					M.put(key, L);
				}
				L.add(e);
			}
		}
	}
	
	public LinkedList<Edge> getEdgesBetween(int orig, int dest) {
		return M.get(new IntPair(orig, dest));
	}
	
	public Edge getEdgeBetween(int orig, int dest) {
		return M.get(new IntPair(orig, dest)).getFirst();
	}
	
	public LinkedList<Edge> getEdgesBetween(String orig, String dest) {
		return getEdgesBetween(g.getIndex(orig), g.getIndex(dest));
	}
	
	public Edge getEdgeBetween(String orig, String dest) {
		return getEdgesBetween(g.getIndex(orig), g.getIndex(dest)).getFirst();
	}
	
	public LinkedList<Edge> getEdges(BitSet edgeSet) {
		LinkedList<Edge> edges = new LinkedList<>();
		for(int v = 0; v < g.V(); v++) {
			for(Edge e : g.outEdges(v)) {
				if(edgeSet.get(e.index())) {
					edges.add(e);
				}
			}
		}
		return edges;
	}
	
	
}
