import os

def get_zoo_sizes():
  sizes = [ ]
  max_name_len = 0
  for fn in os.listdir('topologies/zoo/'):
    max_name_len = max(max_name_len, len(fn))
    f = open('topologies/zoo/' + fn, 'r')
    lines = [line.strip() for line in f.readlines()]
    V = 0
    E = 0
    for line in lines:
      data = line.split(' ')
      if data[0] == 'NODES':
        V = int(data[1])
      if data[0] == 'EDGES':
        E = int(data[1])
    sizes.append( (V, E, fn) )
    f.close()
  sizes.sort()
  f = open('zoo_sizes.txt', 'w')
  for V, E, fn in sizes:
    space = ' ' * (max_name_len + 1 - len(fn))
    f.write('{0}{1}{2}\t{3}\n'.format(fn, space, V, E))
  f.close()

get_zoo_sizes()
