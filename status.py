import os

print('----- STATUS -----')
max_size = 0
info = [ ]
for fn in os.listdir('./logs'):
  if fn.startswith('err'): continue
  name = fn.split('.')
  name = name[0][4:]
  max_size = max(max_size, len(name))
  f = open('./logs/' + fn, 'r')
  lines = [line.strip() for line in f.readlines()]
  last = lines[-1]
  info.append((name, last))
for name, last in info:
  print('{0}:{1}{2}'.format(name, ' ' * (max_size + 1 - len(name)), last))
print('------------------')
