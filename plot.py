import os
import json
import numpy as np
from pylab import *


def read_json(fn):
  f = open(fn, 'r')
  json_data = f.read()
  return json.loads(json_data)

def cdf(data, step):
  data.sort()
  count = 0
  x = [ ]
  y = [ ]
  n = len(data)
  value = min(data)
  i = 0
  while i < n:
    if data[i] <= value:
      count += 1
      i += 1
    else:
      x.append(value)
      y.append(count / n)
      value += step
  x.append(data[-1])
  y.append(1)
  return x, [100 * z for z in y]

def plot_cut_cdfs():
  L = [ ]
  L += os.listdir('topologies/zoo_large/')
  L += os.listdir('topologies/rf/')
  L += os.listdir('topologies/real/')
  for fn in L:
    name = fn.split('.')[0]
    data = read_json('results/topoAnalysis/{0}.json'.format(name))
    cutsizes = [ ]
    for cut_size in data['mincuts']:
      for i in range(data['mincuts'][cut_size]):
        cutsizes.append(int(cut_size))
    cutsizes.sort()
    x, y = cdf(cutsizes, 0.1)
    plot(x, y, label='{0}'.format(name))
  print('plot')
  plt.savefig('./plots/mincuts.eps', format='eps', dpi=1000, bbox_inches='tight')
  
if __name__ == '__main__':
  plot_cut_cdfs()

