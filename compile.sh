#!/bin/bash

if [ -d "./bin" ]; then
  rm -r "./bin"
fi

cp -r "./src" "./bin"
cd "./bin"

find -name '*.java' > sources.txt
javac -cp "../lib/unitTests/*" @sources.txt

find . -name "*.java" -exec rm -f {} \;

rm sources.txt
