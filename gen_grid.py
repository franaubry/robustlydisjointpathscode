import sys

n = int(sys.argv[1])
m = int(sys.argv[2])

f = open('topologies/grid/grid_{0}_{1}.g'.format(n, m), 'w')

idx = 0
for i in range(n):
  for j in range(m):
    for d in [(1, 0), (0, 1)]:
      ii = i + d[0]
      jj = j + d[1]
      if 0 <= ii and ii < n and 0 <= jj and jj < m:
        u = '({0},{1})'.format(i, j)
        v = '({0},{1})'.format(ii, jj)
        f.write('{0} {1} 1 1 1 {2}\n'.format(u, v, idx))
        idx += 1

f.close()
